package br.com.b2seller.request;

import com.google.gson.JsonObject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Arrays;

import br.com.b2seller.AppTest;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.entity.SendEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppRules;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.repository.RuleRepository;
import br.com.b2seller.repository.SendRepository;
import retrofit2.Call;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@RunWith(RobolectricTestRunner.class)
public class RuleTest {
    @Test
    public void fetchAll() {
        try {
            Call<JsonObject> callRules = RuleRepository.fetchAll();
            Call<JsonObject> callSends = SendRepository.fetchAll(AppTest.USER_ID);

            RuleEntity[] rules = Repository.execSync(callRules, RuleEntity[].class);
            JsonObject sends = Repository.execSync(callSends, JsonObject.class);

            for (RuleEntity rule : rules) {
                if (sends.has(String.valueOf(rule.getId()))) {
                    rule.setCount(sends.get(String.valueOf(rule.getId())).getAsInt());
                }
            }

            System.out.println("BEFORE");
            System.out.println(Arrays.toString(rules));

            rules = AppRules.filter(rules);

            System.out.println("AFTER");
            System.out.println(Arrays.toString(rules));

            assertNotNull(rules);
        } catch (ResellerException e) {
            System.out.println(e.getMessage());

            fail(e.getMessage());
        }
    }

    @Test
    public void fetchSends() {
        try {
            Call<JsonObject> call = SendRepository.fetchCostumers(AppTest.USER_ID, 5);
            SendEntity[] sends = Repository.execSync(call, SendEntity[].class);

            System.out.println(Arrays.toString(sends));

            assertNotNull(sends);
        } catch (ResellerException e) {
            System.out.println(e.getMessage());

            fail(e.getMessage());
        }
    }
}
