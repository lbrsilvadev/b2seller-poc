package br.com.b2seller.request;

import com.google.gson.JsonObject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import br.com.b2seller.AppTest;
import br.com.b2seller.entity.UserEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.repository.UserRepository;
import retrofit2.Call;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@RunWith(RobolectricTestRunner.class)
public class LoginTest {
    @Test
    public void success() {
        String email = "consultor@allin.com.br";
        String password = "123456";

        try {
            UserEntity user = Repository.execSync(UserRepository.login(email, password), UserEntity.class);

            System.out.println(user.toString());

            assertNotNull(user);
        } catch (ResellerException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void invalidPassword() {
        String email = "rrodrigues@allin.com.br";
        String password = "Senha inválida";

        try {
            Repository.execSync(UserRepository.login(email, password), UserEntity.class);

            fail("Login não poderia ser válido");
        } catch (ResellerException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void userNotFound() {
        String email = "testando@praver.com.br";
        String password = "123456";

        try {
            Repository.execSync(UserRepository.login(email, password), UserEntity.class);

            fail("Login não poderia ser válido");
        } catch (ResellerException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void update() {
        String deviceToken = "dB5jIkKZShCXrURDneWs66:APA91bHMjeLXd7kmE49KQqcQDL-AlwkJUJeTdTR_j9SUW6wRkNiY9Xe6AJz0HsAanDVuIhjO25-tnUrd8N2esEHhJ2sgMRt0Wb4cklcBakFpNQQogIgSFTdJXLVXOrgY_Niwj1JZ1zzL";
        String language = "pt-BR";

        try {
            Call<JsonObject> call = UserRepository.update(AppTest.USER_ID, deviceToken, language);
            JsonObject json = Repository.execSync(call, JsonObject.class);

            System.out.println(json.toString());

            assertNotNull(json);
        } catch (ResellerException e) {
            fail(e.getMessage());
        }
    }
}
