package br.com.b2seller.request;

import com.google.gson.JsonObject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Arrays;

import br.com.b2seller.AppTest;
import br.com.b2seller.BuildConfig;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.PurchasedEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppUtils;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.repository.CostumerRepository;
import retrofit2.Call;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@RunWith(RobolectricTestRunner.class)
public class CostumerTest {
    @Test
    public void fetchAll() {
        try {
            Call<JsonObject> call = CostumerRepository.fetchAll(AppTest.USER_ID);
            CostumerEntity[] costumers = Repository.execSync(call, CostumerEntity[].class);

            System.out.println(Arrays.toString(costumers));

            assertNotNull(costumers);
        } catch (ResellerException e) {
            System.out.println(e.getMessage());

            fail(e.getMessage());
        }
    }

    @Test
    public void fetchOne() {
        try {
            String email = "fatimadourado@uol.com.br";
            String md5 = AppUtils.md5(email + BuildConfig.CLIENT_ID);

            System.out.println(md5);

            Call<JsonObject> call = CostumerRepository.fetchOne(md5);
            CostumerEntity costumer = Repository.execSync(call, CostumerEntity.class);
            costumer.setId(md5);

            System.out.println(costumer.toString());

            assertNotNull(costumer);
        } catch (ResellerException e) {
            System.out.println(e.getMessage());

            fail(e.getMessage());
        }
    }

    @Test
    public void purchased() {
        try {
            Call<JsonObject> call = CostumerRepository.purchased(AppTest.USER_ID);
            PurchasedEntity[] purchased = Repository.execSync(call, PurchasedEntity[].class);

            System.out.println(Arrays.toString(purchased));

            assertNotNull(purchased);
        } catch (ResellerException e) {
            System.out.println(e.getMessage());

            fail(e.getMessage());
        }
    }
}
