package br.com.b2seller.repository;

import com.google.gson.JsonObject;

import org.junit.Test;

import br.com.b2seller.repository.config.network.JWT;

import static org.junit.Assert.fail;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class JWTTest {
    @Test
    public void jwt() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user", "lbrsilva");
        jsonObject.addProperty("password", "Mudar123");

        String jwt = JWT.encode(jsonObject);
        boolean valid = JWT.isValid(jwt);

        if (valid) {
            System.out.println("Valid Token: " + jwt);
        } else {
            fail("Invalid Token: " + jwt);
        }
    }

    @Test
    public void decode() {
        String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibGJyc2lsdmEiLCJwYXNzd29yZCI6Ik11ZGFyMTIzIn0.hQ3ONVihHcuEVqNwqGWlwhgfvQ1GAWSdiSllpHBaSs0";

        System.out.println(JWT.decode(jwt));
    }
}