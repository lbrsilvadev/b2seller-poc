package br.com.b2seller.helper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class AppValidatorTest {
    @Test
    public void invalidEmail() {
        String user = "lucasbrsilva@gm";

        assertFalse(AppValidator.email(user));
    }

    @Test
    public void validEmail() {
        String user = "lucasbrsilva@gmail.com";

        assertTrue(AppValidator.email(user));
    }

    @Test
    public void invalidPasswordLenght() {
        String password = "123";

        assertFalse(AppValidator.password(password));
    }

    @Test
    public void invalidPasswordEmpty() {
        String password = "";

        assertFalse(AppValidator.password(password));
    }

    @Test
    public void validPassword() {
        String password = "1231231";

        assertTrue(AppValidator.password(password));
    }
}
