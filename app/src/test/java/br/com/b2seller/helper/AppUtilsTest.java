package br.com.b2seller.helper;

import org.junit.Test;

import br.com.b2seller.BuildConfig;

import static org.junit.Assert.assertEquals;

public class AppUtilsTest {
    @Test
    public void md5() {
        String email = "fatimadourado@uol.com.br";
        String md5 = AppUtils.md5(email + BuildConfig.CLIENT_ID);

        System.out.println(md5);

        assertEquals("b61ddefd0d30c750bc67263c4172226c", md5);
    }

    @Test
    public void date() {
        String oldDate = "2020-07-15T03:00:00.000Z";
        String newDate = AppUtils.dateFormat(oldDate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "dd/MM/yyyy");

        System.out.println(newDate);

        assertEquals("15/07/2020", newDate);
    }

    @Test
    public void dateDefault() {
        String oldDate = "2020-07-15";
        String newDate = AppUtils.dateFormat(oldDate, "yyyy-MM-dd", "dd/MM/yyyy");

        System.out.println(newDate);

        assertEquals("15/07/2020", newDate);
    }

    @Test
    public void money() {
        double value = 199.90;
        String money = AppUtils.money(value);

        System.out.println(money);

        assertEquals("R$ 199,00", money);
    }
}
