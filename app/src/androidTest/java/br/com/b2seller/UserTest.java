package br.com.b2seller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.b2seller.entity.UserEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.ui.user.login.LoginViewModel;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserTest {
    @Mock
    private RequestCallback<UserEntity> callback;
    private LoginViewModel viewModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.viewModel = spy(new LoginViewModel());
    }

    @Test
    public void emptyEmail() {
        String email = "";
        String password = "12312312131";

        this.viewModel.login(email, password, this.callback);

        verify(this.callback).onFailure(any(ResellerException.class));
    }

    @Test
    public void invalidEmail() {
        String email = "lucasbrsilva@gmail";
        String password = "12312312131";

        this.viewModel.login(email, password, this.callback);

        verify(this.callback).onFailure(any(ResellerException.class));
    }

    @Test
    public void invalidPassword() {
        String email = "lucasbrsilva@gmail";
        String password = "12";

        this.viewModel.login(email, password, this.callback);

        verify(this.callback).onFailure(any(ResellerException.class));
    }
}
