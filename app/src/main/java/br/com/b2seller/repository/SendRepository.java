package br.com.b2seller.repository;

import com.google.gson.JsonObject;

import br.com.b2seller.helper.Repository;
import br.com.b2seller.repository.network.Send;
import retrofit2.Call;

public class SendRepository {
    public static Call<JsonObject> fetchAll(String id) {
        return Repository.network(Send.class).fetchAll(id);
    }

    public static Call<JsonObject> fetchCostumers(String id, int rule) {
        return Repository.network(Send.class).fetchCostumers(id, rule);
    }
}
