package br.com.b2seller.repository.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface User {
    @POST("auth/login")
    Call<JsonObject> login(@Header("x-data-token") String dataToken);

    @POST("auth/recover")
    Call<JsonObject> recover(@Header("x-data-token") String dataToken);

    @PUT("consultants/{id}")
    Call<JsonObject> update(@Header("x-data-token") String dataToken, @Path("id") String id);

    @GET("banners/active")
    Call<JsonObject> banners();
}
