package br.com.b2seller.repository;

import com.google.gson.JsonObject;

import br.com.b2seller.helper.LocaleHelper;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.repository.network.User;
import retrofit2.Call;

public class UserRepository {
    public static Call<JsonObject> login(String email, String password) {
        JsonObject json = new JsonObject();
        json.addProperty("email", email);
        json.addProperty("password", password);

        return Repository.network(User.class).login(Repository.header(json));
    }

    public static Call<JsonObject> recover(String email) {
        JsonObject json = new JsonObject();
        json.addProperty("email", email);

        return Repository.network(User.class).recover(Repository.header(json));
    }

    public static Call<JsonObject> update(String id, String deviceToken) {
        return UserRepository.update(id, deviceToken, LocaleHelper.getLang());
    }

    public static Call<JsonObject> update(String id, String deviceToken, String language) {
        JsonObject json = new JsonObject();
        json.addProperty("deviceToken", deviceToken);
        json.addProperty("language", language);

        return Repository.network(User.class).update(Repository.header(json), id);
    }

    public static Call<JsonObject> clear(String id) {
        return UserRepository.update(id, "", "");
    }

    public static Call<JsonObject> banners() {
        return Repository.network(User.class).banners();
    }
}
