package br.com.b2seller.repository;

import com.google.gson.JsonObject;

import br.com.b2seller.helper.Repository;
import br.com.b2seller.repository.network.Rule;
import retrofit2.Call;

public class RuleRepository {
    public static Call<JsonObject> fetchAll() {
        return Repository.network(Rule.class).fetchAll();
    }
}
