package br.com.b2seller.repository.config.network;

import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;
import java.security.Key;

import javax.crypto.spec.SecretKeySpec;

import br.com.b2seller.BuildConfig;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;

public class JWT {
    public static String encode(Object object) {
        return JWT.encode(new Gson().toJson(object));
    }

    public static String encode(String json) {
        return Jwts.builder().setPayload(json).signWith(JWT.key(), HS256).compact();
    }

    public static String decode(String jwt) {
        Object body = Jwts.parserBuilder().setSigningKey(JWT.key()).build().parse(jwt).getBody();

        if (!(body instanceof String)) {
            return new Gson().toJson(body);
        }

        return body.toString();
    }

    public static <T> T decode(String jwt, Class<T> clazz) {
        String json = JWT.decode(jwt);

        return new Gson().fromJson(json, clazz);
    }

    public static Key key() {
        byte[] keyBytes = BuildConfig.SECRET_KEY.getBytes(StandardCharsets.UTF_8);

        return new SecretKeySpec(keyBytes, HS256.getJcaName());
    }

    public static boolean isValid(String jwt) {
        try {
            Jwts.parserBuilder().setSigningKey(JWT.key()).build().parseClaimsJws(jwt);

            return true;
        } catch (JwtException e) {
            return false;
        }
    }
}
