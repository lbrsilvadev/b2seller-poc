package br.com.b2seller.repository.config.network;

import android.annotation.SuppressLint;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import br.com.b2seller.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;

public class HttpService {
    @SuppressLint("TrustAllX509TrustManager")
    public static OkHttpClient get() {
        try {
            // Frees connection to any valid ssl
            TrustManager[] trustAllCerts = MyX509TrustManager.get();
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            Builder httpClient = new Builder();
            httpClient.sslSocketFactory(sslSocketFactory, (MyX509TrustManager) trustAllCerts[0]);
            httpClient.hostnameVerifier((hostname, session) -> true);

            // Informs the time out of requests
            httpClient.connectTimeout(BuildConfig.TIME_OUT, TimeUnit.MINUTES);
            httpClient.writeTimeout(BuildConfig.TIME_OUT, TimeUnit.MINUTES);
            httpClient.readTimeout(BuildConfig.TIME_OUT, TimeUnit.MINUTES);

            // Encrypts the request using JWT
            httpClient.addInterceptor(new JWTInterceptor());

            if (BuildConfig.DEBUG) {
                // If in debug mode, displays the request logs
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(logging);
            }

            return httpClient.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
