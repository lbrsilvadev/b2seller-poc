package br.com.b2seller.repository.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Rule {
    @GET("rules")
    Call<JsonObject> fetchAll();
}
