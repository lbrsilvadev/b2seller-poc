package br.com.b2seller.repository.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Log {
    @POST("events")
    Call<JsonObject> events(@Header("x-data-token") String dataToken);
}
