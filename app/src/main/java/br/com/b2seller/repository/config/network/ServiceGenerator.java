package br.com.b2seller.repository.config.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.b2seller.BuildConfig;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by lucasrodrigues on 7/20/16.
 */
public class ServiceGenerator {
    public static <T> T get(Class<T> network) {
        return ServiceGenerator.get(BuildConfig.BASE_URL, network);
    }

    public static <T> T get(String baseUrl, Class<T> network) {
        Gson gson = new GsonBuilder().setLenient().create();

        Builder builder = new Builder();
        builder.baseUrl(baseUrl);
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        builder.addConverterFactory(ScalarsConverterFactory.create());

        return builder.client(HttpService.get()).build().create(network);
    }
}
