package br.com.b2seller.repository.config.network;

import android.annotation.SuppressLint;

import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@SuppressLint("TrustAllX509TrustManager")
public class MyX509TrustManager implements X509TrustManager {
    public static TrustManager[] get() {
        return new TrustManager[] { new MyX509TrustManager() };
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) {

    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) {

    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[]{};
    }
}
