package br.com.b2seller.repository.config.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import br.com.b2seller.BuildConfig;
import br.com.b2seller.ResellerApplication;
import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.repository.database.MessagesDb;

@Database(entities = {MessageEntity.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract MessagesDb messageDao();

    public static void clear() {
        Room.databaseBuilder(ResellerApplication
                .context(), AppDatabase.class, BuildConfig.DB_NAME)
                .allowMainThreadQueries().build().clearAllTables();
    }
}
