package br.com.b2seller.repository.config.network;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import br.com.b2seller.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class JWTInterceptor implements Interceptor {
    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Builder requestBuilder = request.newBuilder();

        requestBuilder.addHeader("Content-Type", "application/json");
        requestBuilder.addHeader("x-access-token", BuildConfig.X_ACCESS_TOKEN);

        return chain.proceed(requestBuilder.build());
    }
}
