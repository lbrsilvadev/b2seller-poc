package br.com.b2seller.repository;

import com.google.gson.JsonObject;

import br.com.b2seller.helper.Repository;
import br.com.b2seller.repository.network.Costumer;
import retrofit2.Call;

public class CostumerRepository {
    public static Call<JsonObject> fetchAll(String id) {
        return Repository.network(Costumer.class).fetchAll(id);
    }

    public static Call<JsonObject> fetchOne(String id) {
        return Repository.network(Costumer.class).fetchOne(id);
    }

    public static Call<JsonObject> purchased(String id) {
        return Repository.network(Costumer.class).purchased(id);
    }
}
