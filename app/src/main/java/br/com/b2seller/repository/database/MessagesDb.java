package br.com.b2seller.repository.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import br.com.b2seller.entity.MessageEntity;

@Dao
public interface MessagesDb {
    @Query("SELECT * FROM messages ORDER BY createdAt DESC")
    List<MessageEntity> fetchAll();

    @Query("SELECT * FROM messages WHERE text LIKE :keyword ORDER BY createdAt DESC")
    List<MessageEntity> find(String keyword);

    @Insert
    void insert(MessageEntity message);

    @Update
    void update(MessageEntity message);

    @Delete
    void delete(MessageEntity message);
}
