package br.com.b2seller.repository.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Costumer {
    @GET("consultants/{id}/clients")
    Call<JsonObject> fetchAll(@Path("id") String id);

    @GET("clients/{id}")
    Call<JsonObject> fetchOne(@Path("id") String id);

    @GET("clients/{id}/purchased")
    Call<JsonObject> purchased(@Path("id") String id);
}
