package br.com.b2seller.repository;

import androidx.room.Room;

import java.util.List;

import br.com.b2seller.BuildConfig;
import br.com.b2seller.ResellerApplication;
import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.repository.config.database.AppDatabase;

public class MessageRepository {
    public static List<MessageEntity> fetchAll() {
        AppDatabase appDatabase = MessageRepository.get();

        return appDatabase.messageDao().fetchAll();
    }

    public static List<MessageEntity> find(String keyword) {
        AppDatabase appDatabase = MessageRepository.get();

        return appDatabase.messageDao().find(keyword);
    }

    public static void insert(MessageEntity message) {
        AppDatabase appDatabase = MessageRepository.get();

        appDatabase.messageDao().insert(message);
    }

    public static void update(MessageEntity message) {
        AppDatabase appDatabase = MessageRepository.get();

        appDatabase.messageDao().update(message);
    }

    public static void delete(MessageEntity message) {
        AppDatabase appDatabase = MessageRepository.get();

        appDatabase.messageDao().delete(message);
    }

    private static AppDatabase get() {
        return Room.databaseBuilder(ResellerApplication.context(),
                AppDatabase.class, BuildConfig.DB_NAME).allowMainThreadQueries().build();
    }
}
