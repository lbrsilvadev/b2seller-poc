package br.com.b2seller.repository.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Send {
    @GET("sends/{id}")
    Call<JsonObject> fetchAll(@Path("id") String id);

    @GET("sends/{id}/rule/{rule}")
    Call<JsonObject> fetchCostumers(@Path("id") String id, @Path("rule") int rule);
}
