package br.com.b2seller;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

import java.lang.ref.WeakReference;

import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.AppRules;
import br.com.b2seller.helper.LocaleHelper;

import br.com.allin.mobile.pushnotification.AlliNPush;

public class ResellerApplication extends MultiDexApplication {
    private static WeakReference<Context> context;

    @Override
    public void onCreate() {
        this.setup();

        super.onCreate();

        AppLog.events("app");
    }

    private void setup() {
        AlliNPush.getInstance().registerForPushNotifications(this);
        ResellerApplication.context(this);
        LocaleHelper.initialize();
        AppRules.load();
    }

    public static Context context() {
        return ResellerApplication.context.get();
    }

    public static void context(Context context) {
        ResellerApplication.context = new WeakReference<>(context);
    }
}
