package br.com.b2seller.identifier;

public interface SharedIdentifier {
    String SHARED_USER = "shared_user";
    String USER = "user";

    String SHARED_RULLES = "shared_rulles";
    String RULES = "rules";
}
