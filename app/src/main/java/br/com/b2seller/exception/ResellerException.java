package br.com.b2seller.exception;

import android.text.TextUtils;

import androidx.annotation.StringRes;

import br.com.b2seller.R;
import br.com.b2seller.ResellerApplication;

public class ResellerException extends Exception {
    private String message;
    private Integer resMessage;

    public ResellerException() {
    }

    public ResellerException(String message) {
        this.message = message;
    }

    public ResellerException(@StringRes int resMessage) {
        this.resMessage = resMessage;
    }

    public String getMessage() {
        if (!TextUtils.isEmpty(this.message)) {
            return this.message;
        }

        if (this.resMessage != null) {
            return ResellerApplication.context().getString(this.resMessage);
        }

        return ResellerApplication.context().getString(R.string.unknown_error);
    }
}
