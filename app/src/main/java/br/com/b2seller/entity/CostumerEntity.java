package br.com.b2seller.entity;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.b2seller.BuildConfig;
import br.com.b2seller.helper.AppUtils;

public class CostumerEntity implements Serializable, Comparable<CostumerEntity> {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("clientAt")
    @Expose
    private String clientAt;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("opportunities")
    @Expose
    private int opportunities;

    private SendEntity send;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return TextUtils.isEmpty(phone) ? "--" : phone;
    }

    public String getClientAt() {
        if (TextUtils.isEmpty(clientAt)) {
            return "--";
        }

        return AppUtils.dateFormat(clientAt, "yyyy-MM-dd", "dd/MM/yyyy");
    }

    public String getBirthday() {
        if (TextUtils.isEmpty(birthday)) {
            return "--";
        }

        return AppUtils.dateFormat(birthday, "yyyy-MM-dd", "dd/MM/yyyy");
    }

    public void setOpportunities(int opportunities) {
        this.opportunities = opportunities;
    }

    public int getOpportunities() {
        return opportunities;
    }

    public boolean isOpportunity() {
        return this.opportunities > 0;
    }

    public String getMd5Id() {
        return AppUtils.md5(this.getEmail() + BuildConfig.CLIENT_ID);
    }

    public String getSendDate() {
        try {
            return this.send.getFormattedDate();
        } catch (Exception e) {
            return null;
        }
    }

    public Date getDate() {
        try {
            return new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(this.getSendDate());
        } catch (Exception e) {
            return new Date();
        }
    }

    public ProductEntity[] getProducts() {
        try {
            return this.send.getData().getProducts().toArray(new ProductEntity[0]);
        } catch (Exception e) {
            return null;
        }
    }

    public ProductEntity[] getRecommendations() {
        try {
            return this.send.getData().getRecommendations().toArray(new ProductEntity[0]);
        } catch (Exception e) {
            return null;
        }
    }

    public void setSend(SendEntity send) {
        this.send = send;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public int compareTo(CostumerEntity costumer) {
        return this.getName().compareToIgnoreCase(costumer.getName());
    }
}
