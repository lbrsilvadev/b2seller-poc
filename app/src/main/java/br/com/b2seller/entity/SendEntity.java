package br.com.b2seller.entity;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.b2seller.BuildConfig;
import br.com.b2seller.helper.AppUtils;

public class SendEntity implements Serializable {
    @SerializedName("id_envio")
    @Expose
    private int id;
    @SerializedName("nm_email")
    @Expose
    private String email;
    @SerializedName("dt_envio")
    @Expose
    private String sendDate;
    @SerializedName("data")
    @Expose
    private DataEntity data;

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFormattedDate() {
        return this.getFormattedDate("dd/MM/yyyy");
    }

    public String getFormattedDate(String format) {
        if (!TextUtils.isEmpty(this.sendDate)) {
            return AppUtils.dateFormat(this.sendDate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", format);
        }

        return this.sendDate;
    }

    public Date getDate() {
        try {
            return new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(this.getFormattedDate());
        } catch (Exception e) {
            return new Date();
        }
    }

    public DataEntity getData() {
        return data;
    }

    public String getMd5Id() {
        return AppUtils.md5(this.getEmail() + BuildConfig.CLIENT_ID);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
