package br.com.b2seller.entity;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import br.com.b2seller.helper.AppUtils;

public class ProductEntity implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("priceNum")
    @Expose
    private Double priceNum;
    private boolean purchased;
    @SerializedName("productLink")
    @Expose
    private String productLink;

    public String getId() {
        return id;
    }

    public String getImage() {
        if (!TextUtils.isEmpty(this.image) && this.image.startsWith("http://")) {
            return image.replace("http://", "https://");
        }

        return image;
    }

    public String getName() {
        if (!TextUtils.isEmpty(this.productName)) {
            return this.productName;
        }

        return name;
    }

    public String getPrice() {
        if (!TextUtils.isEmpty(this.price) && AppUtils.isDouble(this.price)) {
            return AppUtils.money(this.price);
        } else if (this.priceNum != null) {
            return AppUtils.money(this.priceNum);
        }

        if (TextUtils.isEmpty(this.price)) {
            return "--";
        }

        return this.price;
    }

    public void setPurchased(List<PurchasedEntity> purchaseds) {
        for (PurchasedEntity purchased : purchaseds) {
            for (ProductEntity product : purchased.getProducts()) {
                if (product.getId().equals(this.getId())) {
                    this.purchased = true;

                    break;
                }
            }
        }
    }

    public boolean isPurchased() {
        return purchased;
    }
}
