package br.com.b2seller.entity;

import android.graphics.drawable.Drawable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.b2seller.helper.AppRules;

public class RuleEntity implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("name")
    @Expose
    private String name;

    private int count;

    private SendEntity[] sends;

    public int getId() {
        return id;
    }

    public String getAlias() {
        return alias;
    }

    public String getName() {
        return name;
    }

    public boolean hideCount() {
        return AppRules.hideCount(this.alias);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public SendEntity[] getSends() {
        return sends;
    }

    public void setSends(SendEntity[] sends, String id) {
        this.count = 0;

        List<SendEntity> list = new ArrayList<>();

        for (SendEntity send : sends) {
            if (send.getMd5Id().equals(id)) {
                list.add(send);

                this.count += send.getData().getProducts().size();
            }
        }

        this.sends = list.toArray(new SendEntity[0]);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public Drawable getIcon() {
        try {
            return AppRules.icon(this.alias);
        } catch (Exception e) {
            return null;
        }
    }

    public String getTitle() {
        try {
            return AppRules.text(this.alias, true);
        } catch (Exception e) {
            return this.getName();
        }
    }

    public String getDescription() {
        try {
            return AppRules.text(this.alias, false);
        } catch (Exception e) {
            return this.getName();
        }
    }
}
