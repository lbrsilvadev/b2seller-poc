package br.com.b2seller.entity;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import br.com.b2seller.helper.AppUtils;

public class PurchasedEntity implements Serializable {
    @SerializedName("purchaseDate")
    @Expose
    private String purchaseDate;
    @SerializedName("order")
    @Expose
    private String order;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("products")
    @Expose
    private List<ProductEntity> products;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("clientId")
    @Expose
    private String clientId;
    @SerializedName("payment")
    @Expose
    private String payment;

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public String getFormattedPurchaseDate() {
        if (!TextUtils.isEmpty(this.purchaseDate)) {
            return AppUtils.dateFormat(this.purchaseDate, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "dd/MM/yyyy");
        }

        return "--";
    }

    public String getOrder() {
        return order;
    }

    public String getTotal() {
        return total;
    }

    public String getFormattedTotal() {
        if (!TextUtils.isEmpty(this.total)) {
            if (AppUtils.isDouble(this.total)) {
                return AppUtils.money(this.total);
            }

            return total;
        }

        return "--";
    }

    public String getEmail() {
        return email;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }

    public String getStatus() {
        return status;
    }

    public String getFormattedStatus() {
        return status;
    }

    public String getClientId() {
        return clientId;
    }

    public String getPayment() {
        return payment;
    }

    public String getFormattedPayment() {
        return payment;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
