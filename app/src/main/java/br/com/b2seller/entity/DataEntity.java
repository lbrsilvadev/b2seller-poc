package br.com.b2seller.entity;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DataEntity implements Serializable {
    @SerializedName("client")
    @Expose
    private String client;
    @SerializedName("products")
    @Expose
    private List<ProductEntity> products;
    @SerializedName("recommendations")
    @Expose
    private List<ProductEntity> recommendations;

    public String getClient() {
        return client;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }

    public List<ProductEntity> getRecommendations() {
        return recommendations;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
