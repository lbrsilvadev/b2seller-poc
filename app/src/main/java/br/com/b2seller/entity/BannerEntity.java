package br.com.b2seller.entity;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BannerEntity implements Serializable {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("schema")
    @Expose
    private String schema;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("order")
    @Expose
    private int order;

    public String getName() {
        return name;
    }

    public String getImage() {
        if (!TextUtils.isEmpty(this.image)) {
            return this.image.replace("http://", "https://");
        }

        return image;
    }

    public String getSchema() {
        return schema;
    }

    public String getUrl() {
        return url;
    }

    public int getOrder() {
        return order;
    }

    public boolean hasUrl() {
        return !TextUtils.isEmpty(this.url) || !TextUtils.isEmpty(this.schema);
    }

    public String url() {
        if (!TextUtils.isEmpty(this.url)) {
            return this.url;
        } else if (!TextUtils.isEmpty(this.schema)) {
            return this.schema;
        }

        return null;
    }
}
