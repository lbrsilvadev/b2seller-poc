package br.com.b2seller.entity;

import android.net.Uri;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.Date;

import br.com.b2seller.type.EncodeType;

@Entity(tableName = "messages")
public class MessageEntity implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String text;
    private Date createdAt;
    private int discount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public String getTextEncoded(EncodeType encodeType) {
        switch (encodeType) {
            case URI:
                return Uri.encode(this.getText());
            case URL:
                try {
                    return URLEncoder.encode(this.getText(), "utf-8");
                } catch (Exception e) {
                    return this.getText();
                }
        }

        return this.getText();
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
