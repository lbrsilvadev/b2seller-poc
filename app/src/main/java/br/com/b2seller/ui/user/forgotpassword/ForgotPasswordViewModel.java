package br.com.b2seller.ui.user.forgotpassword;

import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;

import br.com.b2seller.R;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppValidator;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.repository.UserRepository;

public class ForgotPasswordViewModel extends ViewModel {
    public void recover(String email, RequestCallback<JsonObject> callback) {
        if (!AppValidator.email(email)) {
            callback.onFailure(new ResellerException(R.string.invalid_email));
        } else {
            Repository.exec(UserRepository.recover(email), callback, JsonObject.class);
        }
    }
}
