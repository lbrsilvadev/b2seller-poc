package br.com.b2seller.ui.user.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import br.com.b2seller.BuildConfig;
import br.com.b2seller.databinding.ActivityLoginBinding;
import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.entity.UserEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.AppRules;
import br.com.b2seller.helper.AppUser;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.helper.base.BaseActivity;
import br.com.b2seller.helper.ui.Alert;
import br.com.b2seller.helper.ui.Load;
import br.com.b2seller.ui.main.MainActivity;
import br.com.b2seller.ui.user.forgotpassword.ForgotPasswordActivity;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> implements RequestCallback<UserEntity> {
    @Override
    public ActivityLoginBinding binding(LayoutInflater layoutInflater) {
        return ActivityLoginBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        this.binding.setActivity(this);
        this.binding.setViewModel(new LoginViewModel());
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        if (BuildConfig.DEBUG) {
            this.binding.etEmail.setText("consultor@allin.com.br");
            this.binding.etPassword.setText("123456");
        }
    }

    public void clickLogin() {
        Load.show(this);

        String user = this.binding.etEmail.getText().toString();
        String password = this.binding.etPassword.getText().toString();

        this.binding.getViewModel().login(user, password, this);
    }

    public void clickForgotPassword() {
        this.startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    @Override
    public void onSuccess(UserEntity user) {
        AppUser.save(user);
        AppRules.load();
        AppLog.events("login", new KeyValueEntity("result", true));

        Load.dismiss();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        this.startActivity(intent);
        this.finish();
    }

    @Override
    public void onFailure(ResellerException exception) {
        AppLog.events("login",
                new KeyValueEntity("success", false),
                new KeyValueEntity("email", this.binding.etEmail.getText().toString()),
                new KeyValueEntity("error", exception.getMessage()));

        Load.dismiss();

        Alert.show(this, exception.getMessage());
    }
}
