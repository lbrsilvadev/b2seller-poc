package br.com.b2seller.ui.main.home;

import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;

import br.com.b2seller.entity.BannerEntity;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.helper.AppUser;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.repository.CostumerRepository;
import br.com.b2seller.repository.UserRepository;
import retrofit2.Call;

public class HomeViewModel extends ViewModel {
    public void loadBanners(RequestCallback<BannerEntity[]> callback) {
        Call<JsonObject> call = UserRepository.banners();
        Repository.exec(call, callback, BannerEntity[].class);
    }

    public void count(OnCostumersCount callback) {
        new Thread(() -> {
            Call<JsonObject> call = CostumerRepository.fetchAll(AppUser.get().getId());

            try {
                CostumerEntity[] costumers = Repository.execSync(call, CostumerEntity[].class);

                int count = 0;

                for (CostumerEntity costumer : costumers) {
                    if (costumer.getOpportunities() > 0) {
                        count++;
                    }
                }

                callback.onCount(count);
            } catch (Exception e) {
                callback.onCount(0);
            }
        }).start();
    }

    interface OnCostumersCount {
        void onCount(int count);
    }
}
