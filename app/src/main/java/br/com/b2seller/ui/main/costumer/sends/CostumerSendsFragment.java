package br.com.b2seller.ui.main.costumer.sends;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import java.util.List;

import br.com.b2seller.databinding.FragmentCostumerSendsBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.PurchasedEntity;
import br.com.b2seller.entity.SendEntity;
import br.com.b2seller.helper.AppUtils;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.identifier.BundleIdentifier;

public class CostumerSendsFragment extends BaseFragment<FragmentCostumerSendsBinding> {
    private List<PurchasedEntity> purchaseds;
    private List<SendEntity> sends;

    @Override
    public FragmentCostumerSendsBinding binding(LayoutInflater layoutInflater) {
        return FragmentCostumerSendsBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        if (bundle != null) {
            this.purchaseds = AppUtils.getList(bundle, BundleIdentifier.BUNDLE_PURCHASEDS);
            this.sends = AppUtils.getList(bundle, BundleIdentifier.BUNDLE_SENDS);
            this.binding.setCostumer((CostumerEntity) bundle.getSerializable(BundleIdentifier.BUNDLE_COSTUMER));
            this.binding.setProductsTitle(bundle.getString(BundleIdentifier.BUNDLE_PRODUCTS_TITLE, ""));
        }

        this.binding.setFragment(this);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        SendAdapter adapter = new SendAdapter(this.sends);
        adapter.setTitle(this.binding.getProductsTitle());
        adapter.setCostumer(this.binding.getCostumer());
        adapter.setPurchaseds(this.purchaseds);

        this.binding.rvProducts.setAdapter(adapter);
    }
}
