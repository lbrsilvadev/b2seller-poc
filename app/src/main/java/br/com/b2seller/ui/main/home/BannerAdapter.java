package br.com.b2seller.ui.main.home;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import br.com.b2seller.R;
import br.com.b2seller.databinding.AdapterBannerBinding;
import br.com.b2seller.databinding.AdapterBannerMainBinding;
import br.com.b2seller.entity.BannerEntity;
import br.com.b2seller.helper.base.BaseViewHolder;
import br.com.b2seller.ui.main.MainActivity;

@SuppressWarnings("all")
public class BannerAdapter extends RecyclerView.Adapter {
    private BannerEntity[] banners;
    private int count;

    public BannerAdapter(BannerEntity[] banners, int count) {
        this.banners = banners;
        this.count = count;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        if (viewType == 0) {
            return new BannerMainViewHolder(AdapterBannerMainBinding.inflate(layoutInflater, parent, false));
        } else {
            return new BannerViewHolder(AdapterBannerBinding.inflate(layoutInflater, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BannerMainViewHolder) {
            BannerMainViewHolder bannerMainViewHolder = (BannerMainViewHolder) holder;

            String textCount = bannerMainViewHolder.activity().getString(R.string.count_costumers, this.count);

            bannerMainViewHolder.getBinding().setBanner(this.banners[position]);
            bannerMainViewHolder.getBinding().tvCostumersCount.setText(textCount);
            bannerMainViewHolder.getBinding().btCheckout.setOnClickListener(v -> {
                MainActivity main = (MainActivity) v.getContext();
                main.changeNavigation(R.id.navigation_opportunity);
            });
        } else {
            BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
            bannerViewHolder.getBinding().setBanner(this.banners[position]);

            if (bannerViewHolder.getBinding().getBanner().hasUrl()) {
                bannerViewHolder.getBinding().ivBanner.setOnClickListener(v -> {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(bannerViewHolder.getBinding().getBanner().url()));
                    v.getContext().startActivity(intent);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.banners.length;
    }

    @BindingAdapter("loadImage")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).into(view);
    }

    static class BannerMainViewHolder extends BaseViewHolder<AdapterBannerMainBinding> {
        BannerMainViewHolder(AdapterBannerMainBinding binding) {
            super(binding);
        }
    }

    static class BannerViewHolder extends BaseViewHolder<AdapterBannerBinding> {
        BannerViewHolder(AdapterBannerBinding binding) {
            super(binding);
        }
    }
}
