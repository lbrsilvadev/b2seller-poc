package br.com.b2seller.ui.main.config.message.create;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import br.com.b2seller.R;
import br.com.b2seller.databinding.FragmentCreateMessageBinding;
import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.helper.AppUtils;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.identifier.BundleIdentifier;

public class CreateMessageFragment extends BaseFragment<FragmentCreateMessageBinding> {
    private MessageEntity message;

    @Override
    public FragmentCreateMessageBinding binding(LayoutInflater layoutInflater) {
        return FragmentCreateMessageBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        if (bundle != null && bundle.containsKey(BundleIdentifier.BUNDLE_MESSAGE)) {
            this.message = (MessageEntity) bundle.getSerializable(BundleIdentifier.BUNDLE_MESSAGE);
        }

        this.binding.setFragment(this);
        this.binding.setMessage(this.message);
        this.binding.setViewModel(new CreateMessageViewModel());
        this.binding.getViewModel().setDiscount(this.message != null ? this.message.getDiscount() : 0);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        this.binding.getViewModel().discount.observe(this.getViewLifecycleOwner(), this::tintDiscount);
        this.binding.getViewModel().saved.observe(this.getViewLifecycleOwner(), saved -> {
            if (saved) {
                MessageSavedDialog.show(this);
            }
        });
    }

    private void tintDiscount(int discount) {
        int colorDisable = ContextCompat.getColor(this.activity(), R.color.hex_AAAAAA);
        int colorEnable = ContextCompat.getColor(this.activity(), R.color.main_color);

        if (discount == 0) {
            this.binding.btFivePercentDiscount.setBackgroundResource(R.drawable.stroke_border_gray);
            this.binding.btFivePercentDiscount.setTextColor(colorDisable);

            this.binding.btTenPercentDiscount.setBackgroundResource(R.drawable.stroke_border_gray);
            this.binding.btTenPercentDiscount.setTextColor(colorDisable);
        } else if (discount == 5) {
            this.binding.btFivePercentDiscount.setBackgroundResource(R.drawable.stroke_border_main);
            this.binding.btFivePercentDiscount.setTextColor(colorEnable);

            this.binding.btTenPercentDiscount.setBackgroundResource(R.drawable.stroke_border_gray);
            this.binding.btTenPercentDiscount.setTextColor(colorDisable);
        } else if (discount == 10) {
            this.binding.btFivePercentDiscount.setBackgroundResource(R.drawable.stroke_border_gray);
            this.binding.btFivePercentDiscount.setTextColor(colorDisable);

            this.binding.btTenPercentDiscount.setBackgroundResource(R.drawable.stroke_border_main);
            this.binding.btTenPercentDiscount.setTextColor(colorEnable);
        }

        this.handleMessage(discount);
    }

    private void handleMessage(int discount) {
        this.binding.setDiscount(discount);

        if (this.message != null) {
            this.message.setDiscount(discount);
        }

        String text = this.binding.etMessage.getText().toString();

        if (text.endsWith(this.percentageText(5))) {
            text = AppUtils.replaceLast(text, this.percentageText(5), "").trim();
        }

        if (text.endsWith(this.percentageText(10))) {
            text = AppUtils.replaceLast(text, this.percentageText(10), "").trim();
        }

        if (discount != 0) {
            text = text.concat(" ").concat(this.percentageText(discount));
        }

        this.binding.etMessage.setText(text);
    }

    private String percentageText(int discount) {
        return this.getString(R.string.with_discount, discount + "%");
    }
}
