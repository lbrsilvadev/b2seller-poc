package br.com.b2seller.ui.main.config.language;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import br.com.b2seller.databinding.FragmentLanguageBinding;
import br.com.b2seller.helper.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class LanguageFragment extends BaseFragment<FragmentLanguageBinding> {
    @Override
    public FragmentLanguageBinding binding(LayoutInflater layoutInflater) {
        return FragmentLanguageBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        this.binding.setFragment(this);
        this.binding.setViewModel(new LanguageViewModel());
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        this.binding.getViewModel().language.observe(this.getViewLifecycleOwner(),
                language -> LanguageChangedDialog.show(this.getActivity(), language));
    }
}
