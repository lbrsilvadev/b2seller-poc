package br.com.b2seller.ui.main.config.message.messages;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import br.com.b2seller.R;
import br.com.b2seller.databinding.FragmentMessagesBinding;
import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.helper.base.BaseFragment;

public class MessagesFragment extends BaseFragment<FragmentMessagesBinding> {
    @Override
    public FragmentMessagesBinding binding(LayoutInflater layoutInflater) {
        return FragmentMessagesBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        this.binding.setFragment(this);
        this.binding.setViewModel(new MessagesViewModel());
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        this.binding.getViewModel().messages.observe(this.getViewLifecycleOwner(), messages -> {
            if (this.binding.rvMessages.getAdapter() != null) {
                ((MessageAdapter) this.binding.rvMessages.getAdapter()).setMessages(messages);
            } else {
                this.binding.rvMessages.setAdapter(new MessageAdapter(this, messages, true));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        this.binding.getViewModel().find(this.binding.etSearch.getText().toString());
    }

    public void delete(MessageEntity message) {
        this.binding.getViewModel().delete(message);
        this.binding.getViewModel().find(this.binding.etSearch.getText().toString());

        MessageDeletedDialog.show(this.activity());
    }

    public void newMessage() {
        this.navigate(R.id.action_create_message);
    }
}
