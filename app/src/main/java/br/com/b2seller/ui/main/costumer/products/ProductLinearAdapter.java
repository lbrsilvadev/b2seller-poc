package br.com.b2seller.ui.main.costumer.products;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.b2seller.databinding.AdapterLinearProductBinding;
import br.com.b2seller.entity.ProductEntity;
import br.com.b2seller.entity.PurchasedEntity;
import br.com.b2seller.helper.base.BaseViewHolder;

public class ProductLinearAdapter extends RecyclerView.Adapter<ProductLinearAdapter.ProductLinearViewHolder> {
    private List<ProductEntity> products;

    ProductLinearAdapter(List<ProductEntity> products) {
        this.products = products;
    }

    public ProductLinearAdapter setPurchaseds(List<PurchasedEntity> purchaseds) {
        if (purchaseds != null) {
            for (ProductEntity product : products) {
                product.setPurchased(purchaseds);
            }
        }

        return this;
    }

    @NonNull
    @Override
    public ProductLinearViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterLinearProductBinding binding = AdapterLinearProductBinding.inflate(layoutInflater, parent, false);

        return new ProductLinearViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductLinearViewHolder holder, int position) {
        holder.getBinding().setProduct(this.products.get(position));
    }

    @Override
    public int getItemCount() {
        return this.products.size();
    }

    @BindingAdapter("loadImage")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).into(view);
    }

    static class ProductLinearViewHolder extends BaseViewHolder<AdapterLinearProductBinding> {
        ProductLinearViewHolder(AdapterLinearProductBinding binding) {
            super(binding);
        }
    }
}
