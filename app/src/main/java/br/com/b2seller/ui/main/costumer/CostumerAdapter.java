package br.com.b2seller.ui.main.costumer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.b2seller.R;
import br.com.b2seller.databinding.AdapterCostumerBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.ArrayConstumer;
import br.com.b2seller.helper.base.BaseViewHolder;
import br.com.b2seller.identifier.BundleIdentifier;

public class CostumerAdapter extends RecyclerView.Adapter<CostumerAdapter.ClientListViewHolder> {
    private String title;
    private List<CostumerEntity> costumers;
    private List<CostumerEntity> originalCostumers;
    private RuleEntity rule;

    public CostumerAdapter(List<CostumerEntity> costumers, List<CostumerEntity> originalCostumers, String title, RuleEntity rule) {
        this.costumers = costumers;
        this.originalCostumers = originalCostumers;
        this.title = title;
        this.rule = rule;
    }

    public void setCostumers(List<CostumerEntity> costumers) {
        this.costumers = costumers;

        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ClientListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterCostumerBinding binding = AdapterCostumerBinding.inflate(layoutInflater, parent, false);

        return new ClientListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientListViewHolder holder, int position) {
        holder.getBinding().setCostumer(this.costumers.get(position));

        holder.itemView.setOnClickListener(view -> {
            CostumerEntity costumer = this.costumers.get(position);
            List<CostumerEntity> costumers = ArrayConstumer.get(this.originalCostumers, costumer.getMd5Id());

            if (this.rule != null) {
                AppLog.events("opportunity",
                        new KeyValueEntity("ruleAlias", this.rule.getAlias()),
                        new KeyValueEntity("costumerId", costumer.getId()));
            } else {
                AppLog.events("clients",
                        new KeyValueEntity("costumerId", costumer.getId()));
            }

            Bundle bundle = new Bundle();
            bundle.putSerializable(BundleIdentifier.BUNDLE_PRODUCTS_TITLE, this.title);

            if (costumers.size() > 1) {
                bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMERS, costumers.toArray(new CostumerEntity[0]));
                Navigation.findNavController(view).navigate(R.id.action_costumer_opportunity, bundle);
            } else {
                bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMER, this.costumers.get(position));
                Navigation.findNavController(view).navigate(R.id.action_costumer_details, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.costumers.size();
    }

    static class ClientListViewHolder extends BaseViewHolder<AdapterCostumerBinding> {
        ClientListViewHolder(AdapterCostumerBinding binding) {
            super(binding);
        }
    }
}
