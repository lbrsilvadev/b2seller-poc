package br.com.b2seller.ui.main.costumer.opportunity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import java.util.List;

import br.com.b2seller.R;
import br.com.b2seller.databinding.AdapterCostumerOpportunityBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.ProductEntity;
import br.com.b2seller.entity.PurchasedEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.ArrayConstumer;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.helper.base.BaseViewHolder;
import br.com.b2seller.helper.ui.Load;
import br.com.b2seller.identifier.BundleIdentifier;
import br.com.b2seller.ui.main.opportunity.OpportunityViewModel;

public class CostumerOpportunityAdapter extends Adapter<CostumerOpportunityAdapter.CostumerOpportunityViewHolder> {
    private List<CostumerEntity> costumers;
    private String productsTitle;

    public CostumerOpportunityAdapter(List<CostumerEntity> costumers, String productsTitle) {
        this.costumers = ArrayConstumer.orderByDate(costumers);
        this.productsTitle = productsTitle;
    }

    @NonNull
    @Override
    public CostumerOpportunityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        AdapterCostumerOpportunityBinding binding = AdapterCostumerOpportunityBinding.inflate(layoutInflater, parent, false);

        return new CostumerOpportunityViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CostumerOpportunityViewHolder holder, int position) {
        holder.getBinding().setCostumer(this.costumers.get(position));

        holder.itemView.setOnClickListener(view -> {
            Load.show(holder.activity());

            OpportunityViewModel viewModel = new OpportunityViewModel();
            viewModel.loadPurchaseds(new RequestCallback<PurchasedEntity[]>() {
                @Override
                public void onSuccess(PurchasedEntity[] response) {
                    this.showProducts(response);
                }

                @Override
                public void onFailure(ResellerException exception) {
                    this.showProducts(null);
                }

                public void showProducts(PurchasedEntity[] purchaseds) {
                    Load.dismiss();

                    CostumerEntity costumer = holder.getBinding().getCostumer();
                    ProductEntity[] products = costumer.getProducts();
                    ProductEntity[] recommendations = costumer.getRecommendations();

                    Bundle bundle = new Bundle();
                    bundle.putSerializable(BundleIdentifier.BUNDLE_PRODUCTS_TITLE, productsTitle);
                    bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMER, costumer);
                    bundle.putSerializable(BundleIdentifier.BUNDLE_PURCHASEDS, purchaseds);
                    bundle.putSerializable(BundleIdentifier.BUNDLE_PRODUCTS, products);
                    bundle.putSerializable(BundleIdentifier.BUNDLE_RECOMMENDATIONS, recommendations);

                    Navigation.findNavController(view).navigate(R.id.action_costumer_products, bundle);
                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return this.costumers.size();
    }

    static class CostumerOpportunityViewHolder extends BaseViewHolder<AdapterCostumerOpportunityBinding> {
        CostumerOpportunityViewHolder(AdapterCostumerOpportunityBinding binding) {
            super(binding);
        }
    }
}
