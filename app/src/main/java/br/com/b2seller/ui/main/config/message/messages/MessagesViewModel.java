package br.com.b2seller.ui.main.config.message.messages;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.repository.MessageRepository;

public class MessagesViewModel extends ViewModel {
    MutableLiveData<List<MessageEntity>> messages = new MutableLiveData<>();

    public void find(String keyword) {
        this.messages.postValue(MessageRepository.find("%" + keyword + "%"));
    }

    public void delete(MessageEntity message) {
        MessageRepository.delete(message);
    }
}
