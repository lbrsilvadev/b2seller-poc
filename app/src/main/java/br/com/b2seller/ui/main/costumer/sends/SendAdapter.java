package br.com.b2seller.ui.main.costumer.sends;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.b2seller.R;
import br.com.b2seller.databinding.AdapterSendBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.ProductEntity;
import br.com.b2seller.entity.PurchasedEntity;
import br.com.b2seller.entity.SendEntity;
import br.com.b2seller.helper.ArraySend;
import br.com.b2seller.helper.base.BaseViewHolder;
import br.com.b2seller.identifier.BundleIdentifier;

public class SendAdapter extends RecyclerView.Adapter<SendAdapter.SendViewHolder> {
    private List<SendEntity> sends;
    private List<PurchasedEntity> purchaseds;

    private String title;
    private CostumerEntity costumer;

    public SendAdapter(List<SendEntity> sends) {
        this.sends = ArraySend.orderByDate(sends);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCostumer(CostumerEntity costumer) {
        this.costumer = costumer;
    }

    public void setPurchaseds(List<PurchasedEntity> purchaseds) {
        this.purchaseds = purchaseds;
    }

    @NonNull
    @Override
    public SendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterSendBinding binding = AdapterSendBinding.inflate(layoutInflater, parent, false);

        return new SendAdapter.SendViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SendViewHolder holder, int position) {
        holder.getBinding().setSend(this.sends.get(position));

        holder.itemView.setOnClickListener(view -> this.click(view, this.sends.get(position)));
    }

    @Override
    public int getItemCount() {
        return this.sends.size();
    }

    public void click(View view, SendEntity send) {
        ProductEntity[] products = send.getData().getProducts().toArray(new ProductEntity[0]);
        ProductEntity[] recommendations = send.getData().getRecommendations().toArray(new ProductEntity[0]);

        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleIdentifier.BUNDLE_PRODUCTS_TITLE, this.title);
        bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMER, this.costumer);
        bundle.putSerializable(BundleIdentifier.BUNDLE_PRODUCTS, products);
        bundle.putSerializable(BundleIdentifier.BUNDLE_RECOMMENDATIONS, recommendations);
        bundle.putSerializable(BundleIdentifier.BUNDLE_PURCHASEDS, this.purchaseds.toArray(new PurchasedEntity[0]));

        Navigation.findNavController(view).navigate(R.id.action_costumer_products, bundle);
    }

    static class SendViewHolder extends BaseViewHolder<AdapterSendBinding> {
        SendViewHolder(AdapterSendBinding binding) {
            super(binding);
        }
    }
}
