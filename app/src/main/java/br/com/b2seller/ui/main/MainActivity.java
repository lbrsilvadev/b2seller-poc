package br.com.b2seller.ui.main;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import br.com.allin.mobile.pushnotification.AlliNPush;
import br.com.b2seller.R;
import br.com.b2seller.databinding.ActivityMainBinding;
import br.com.b2seller.helper.AppUser;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.helper.base.BaseActivity;
import br.com.b2seller.repository.UserRepository;

public class MainActivity extends BaseActivity<ActivityMainBinding> {
    @Override
    public ActivityMainBinding binding(LayoutInflater layoutInflater) {
        return ActivityMainBinding.inflate(layoutInflater);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        this.setupNavigation();
        this.verifyScheme(bundle);

        this.update();
    }

    private void update() {
        String deviceToken = AlliNPush.getInstance().getDeviceToken();
        Repository.exec(UserRepository.update(AppUser.get().getId(), deviceToken));
    }

    private void verifyScheme(@Nullable Bundle bundle) {
        if (bundle != null) {
            Uri uri = Uri.parse(bundle.getString("url_scheme"));
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            navController.navigate(uri);
        }
    }

    private void setupNavigation() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(this.binding.navView, navController);
    }

    @Override
    public void onBackPressed() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        if (!navController.popBackStack()) {
            this.finish();
        }
    }

    public void changeNavigation(@IdRes int itemId) {
        this.binding.navView.setSelectedItemId(itemId);
    }
}
