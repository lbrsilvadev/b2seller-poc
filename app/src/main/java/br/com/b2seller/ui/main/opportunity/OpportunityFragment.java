package br.com.b2seller.ui.main.opportunity;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import br.com.b2seller.databinding.FragmentOpportunityBinding;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.helper.ui.Load;

public class OpportunityFragment extends BaseFragment<FragmentOpportunityBinding> implements RequestCallback<RuleEntity[]> {
    @Override
    public FragmentOpportunityBinding binding(LayoutInflater layoutInflater) {
        return FragmentOpportunityBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        this.binding.setFragment(this);
        this.binding.setViewModel(new OpportunityViewModel());

        AppLog.events("opportunity");
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        Load.show(this.activity());

        this.binding.rvOpportunities.setLayoutManager(new GridLayoutManager(this.activity(), 2));
        this.binding.getViewModel().loadRules(this);
    }

    @Override
    public void onSuccess(RuleEntity[] rules) {
        this.setupOpportunities(rules);
    }

    @Override
    public void onFailure(ResellerException exception) {
        this.setupOpportunities(null);
    }

    private void setupOpportunities(RuleEntity[] rules) {
        mainThread(() -> {
            Load.dismiss();

            if (rules != null && rules.length > 0) {
                if (this.binding.rvOpportunities.getAdapter() != null) {
                    ((OpportunityAdapter) this.binding.rvOpportunities.getAdapter()).setRules(rules);
                } else {
                    this.binding.rvOpportunities.setAdapter(new OpportunityAdapter(rules));
                }
            } else {
                this.binding.rvOpportunities.setAdapter(null);
            }
        });
    }
}
