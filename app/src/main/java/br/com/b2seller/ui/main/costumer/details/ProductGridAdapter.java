package br.com.b2seller.ui.main.costumer.details;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.b2seller.databinding.AdapterGridProductsBinding;
import br.com.b2seller.entity.ProductEntity;
import br.com.b2seller.helper.base.BaseViewHolder;

public class ProductGridAdapter extends RecyclerView.Adapter<ProductGridAdapter.ProductGridViewHolder> {
    private List<ProductEntity> products;

    ProductGridAdapter(List<ProductEntity> products) {
        this.products = products;
    }

    public void setProducts(List<ProductEntity> products) {
        this.products = products;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductGridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterGridProductsBinding binding = AdapterGridProductsBinding.inflate(layoutInflater, parent, false);

        return new ProductGridViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductGridViewHolder holder, int position) {
        holder.getBinding().setProduct(this.products.get(position));
    }

    @Override
    public int getItemCount() {
        return this.products.size();
    }

    @BindingAdapter("loadImage")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).into(view);
    }

    static class ProductGridViewHolder extends BaseViewHolder<AdapterGridProductsBinding> {
        ProductGridViewHolder(AdapterGridProductsBinding binding) {
            super(binding);
        }
    }
}
