package br.com.b2seller.ui.main.costumer.details;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import br.com.b2seller.R;
import br.com.b2seller.databinding.FragmentCostumerDetailsBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.entity.PurchasedEntity;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.identifier.BundleIdentifier;
import br.com.b2seller.ui.main.opportunity.OpportunityViewModel;

public class CostumerDetailsFragment extends BaseFragment<FragmentCostumerDetailsBinding> {
    private PurchasedEntity[] purchaseds;

    public PurchasedEntity[] getPurchaseds() {
        return purchaseds;
    }

    public void setPurchaseds(PurchasedEntity[] purchaseds) {
        this.purchaseds = purchaseds;
    }

    @Override
    public FragmentCostumerDetailsBinding binding(LayoutInflater layoutInflater) {
        return FragmentCostumerDetailsBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        if (bundle != null) {
            this.binding.setCostumer((CostumerEntity) bundle.getSerializable(BundleIdentifier.BUNDLE_COSTUMER));
        }

        this.binding.setOpportunityViewModel(new OpportunityViewModel());
        this.binding.setFragment(this);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        this.binding.rvOpportunities.setLayoutManager(new GridLayoutManager(this.activity(), 2));
        this.binding.rvOpportunities.setNestedScrollingEnabled(false);
        this.binding.rvProducts.setLayoutManager(new GridLayoutManager(this.activity(), 2));
        this.binding.rvProducts.setNestedScrollingEnabled(false);

        String id = this.binding.getCostumer().getMd5Id();

        this.binding.getOpportunityViewModel().loadRules(id, new RequestCallback<RuleEntity[]>() {
            @Override
            public void onSuccess(RuleEntity[] rules) {
                this.setupOpportunities(rules);
            }

            @Override
            public void onFailure(ResellerException exception) {
                this.setupOpportunities(null);
            }

            private void setupOpportunities(RuleEntity[] rules) {
                mainThread(() -> {
                    if (rules != null && rules.length > 0) {
                        if (binding.rvOpportunities.getAdapter() != null) {
                            ((OpportunitySmallAdapter) binding.rvOpportunities.getAdapter()).setRules(rules);
                        } else {
                            CostumerEntity costumer = binding.getCostumer();

                            binding.rvOpportunities.setAdapter(new OpportunitySmallAdapter(CostumerDetailsFragment.this, costumer, rules));
                        }
                    } else {
                        binding.rvOpportunities.setAdapter(null);
                    }
                });
            }
        });

        this.binding.getOpportunityViewModel().loadPurchaseds(new RequestCallback<PurchasedEntity[]>() {
            @Override
            public void onSuccess(PurchasedEntity[] response) {
                setPurchaseds(response);

                if (response.length > 0) {
                    binding.setPurchased(response[0]);

                    if (binding.rvProducts.getAdapter() != null) {
                        ((ProductGridAdapter) binding.rvProducts.getAdapter()).setProducts(binding.getPurchased().getProducts());
                    } else {
                        binding.rvProducts.setAdapter(new ProductGridAdapter(binding.getPurchased().getProducts()));
                    }
                }
            }

            @Override
            public void onFailure(ResellerException exception) {
                binding.setPurchased(null);
            }
        });
    }

    public void clickContact(CostumerEntity costumer) {
        AppLog.events("contact",
                new KeyValueEntity("costumerId", costumer.getId()),
                new KeyValueEntity("from", "client-details"));

        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMER, costumer);

        this.navigate(R.id.action_costumer_contact, bundle);
    }
}
