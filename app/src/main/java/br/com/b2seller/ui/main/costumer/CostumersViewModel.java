package br.com.b2seller.ui.main.costumer;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.entity.SendEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppUser;
import br.com.b2seller.helper.ArrayConstumer;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.repository.CostumerRepository;
import br.com.b2seller.repository.SendRepository;
import retrofit2.Call;

public class CostumersViewModel extends ViewModel {
    private List<CostumerEntity> costumers;

    MutableLiveData<List<CostumerEntity>> liveDataCostumers = new MutableLiveData<>();

    public void setCostumers(List<CostumerEntity> costumers) {
        Collections.sort(costumers);

        this.costumers = costumers;
        this.liveDataCostumers.postValue(ArrayConstumer.unique(this.costumers));
    }

    public List<CostumerEntity> getCostumers() {
        return costumers;
    }

    @SuppressWarnings("all")
    public void load(RuleEntity rule, RequestCallback callback) {
        if (rule != null) {
            Call<JsonObject> call = SendRepository.fetchCostumers(AppUser.get().getId(), rule.getId());
            Repository.exec(call, callback, SendEntity[].class);
        } else {
            Call<JsonObject> call = CostumerRepository.fetchAll(AppUser.get().getId());
            Repository.exec(call, callback, CostumerEntity[].class);
        }
    }

    @SuppressWarnings("all")
    public void parseSends(SendEntity[] sends) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Gson gson = new Gson();
                    List<String> jsonList = new ArrayList<>();

                    Call<JsonObject> call = CostumerRepository.fetchAll(AppUser.get().getId());
                    CostumerEntity[] costumers = Repository.execSync(call, CostumerEntity[].class);

                    for (SendEntity send : sends) {
                        CostumerEntity costumer = getCostumer(costumers, send.getMd5Id());

                        if (costumer != null) {
                            costumer.setOpportunities(0);
                            costumer.setSend(send);

                            jsonList.add(gson.toJson(costumer));
                        }
                    }

                    List<CostumerEntity> list = new ArrayList<>();

                    for (String json : jsonList) {
                        list.add(gson.fromJson(json, CostumerEntity.class));
                    }

                    setCostumers(list);
                } catch (ResellerException exception) {
                    setCostumers(new ArrayList<>());
                }
            }
        }).start();
    }

    private CostumerEntity getCostumer(CostumerEntity[] costumers, String md5Id) {
        for (CostumerEntity costumer : costumers) {
            if (costumer.getMd5Id().equals(md5Id)) {
                return costumer;
            }
        }

        return null;
    }

    public void parseCostumers(CostumerEntity[] costumers) {
        this.setCostumers(Arrays.asList(costumers));
    }

    public void find(String keyword) {
        List<CostumerEntity> costumers = ArrayConstumer.unique(this.costumers);
        List<CostumerEntity> list = new ArrayList<>();

        for (CostumerEntity costumer : costumers) {
            if (costumer.getName().toLowerCase().contains(keyword.toLowerCase())) {
                list.add(costumer);
            }
        }

        this.liveDataCostumers.postValue(list);
    }
}
