package br.com.b2seller.ui.main.config.language;

import android.app.Activity;
import android.app.Dialog;
import android.widget.TextView;

import br.com.b2seller.R;
import br.com.b2seller.helper.LocaleHelper;
import br.com.b2seller.helper.base.BaseDialog;

class LanguageChangedDialog {
    static void show(Activity activity, String language) {
        Dialog dialog = BaseDialog.create(activity, R.layout.dialog_language_changed);

        LanguageChangedDialog.setup(activity, dialog, language);

        dialog.show();
    }

    private static void setup(Activity activity, Dialog dialog, String language) {
        TextView tvMessage = dialog.findViewById(R.id.tvMessage);
        TextView tvClose = dialog.findViewById(R.id.tvClose);

        dialog.findViewById(R.id.close).setOnClickListener(v -> {
            dialog.cancel();
            activity.recreate();
        });

        tvMessage.setText(LocaleHelper.getResources(language).getText(R.string.language_changed));
        tvClose.setText(LocaleHelper.getResources(language).getText(R.string.close));
    }
}
