package br.com.b2seller.ui.main.costumer.message;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import br.com.b2seller.R;
import br.com.b2seller.databinding.FragmentCostumerMessageBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.identifier.BundleIdentifier;
import br.com.b2seller.type.EncodeType;
import br.com.b2seller.type.MessageType;
import br.com.b2seller.ui.main.config.message.messages.MessageAdapter;

public class CostumerMessageFragment extends BaseFragment<FragmentCostumerMessageBinding> {
    private MessageType messageType;

    @Override
    public FragmentCostumerMessageBinding binding(LayoutInflater layoutInflater) {
        return FragmentCostumerMessageBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        if (bundle != null) {
            this.messageType = (MessageType) bundle.getSerializable(BundleIdentifier.BUNDLE_MESSAGE_TYPE);
            this.binding.setCostumer((CostumerEntity) bundle.getSerializable(BundleIdentifier.BUNDLE_COSTUMER));
        }

        this.binding.setViewModel(new CostumerMessageViewModel());
        this.binding.setFragment(this);
        this.binding.getViewModel().messages.observe(this.getViewLifecycleOwner(), messages -> {
            if (this.binding.rvMessages.getAdapter() != null) {
                ((MessageAdapter) this.binding.rvMessages.getAdapter()).setMessages(messages);
            } else {
                this.binding.rvMessages.setAdapter(new MessageAdapter(this, messages, false));
            }
        });
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
    }

    @Override
    public void onResume() {
        super.onResume();

        this.binding.getViewModel().fetchAll();
    }

    public void send(MessageEntity message) {
        AppLog.events("contact",
                new KeyValueEntity("costumerId", this.binding.getCostumer().getId()),
                new KeyValueEntity("action", "send-message"),
                new KeyValueEntity("channel", this.messageType.toString()));

        switch (this.messageType) {
            case EMAIL:
                this.email(message);
                break;
            case WHATSAPP:
                this.whatsapp(message);
                break;
        }
    }

    public void newMessage() {
        AppLog.events("contact",
                new KeyValueEntity("costumerId", this.binding.getCostumer().getId()),
                new KeyValueEntity("action", "new-message"));

        this.navigate(R.id.action_create_message);
    }

    private void email(MessageEntity message) {
        String email = this.binding.getCostumer().getEmail();
        String url = "mailto:" + email + "?body=" + message.getTextEncoded(EncodeType.URI);
        Uri uri = Uri.parse(url);

        this.startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, uri), null));
    }

    private void whatsapp(MessageEntity message) {
        String url = "https://api.whatsapp.com/send?phone=%s&text=%s";
        String phone = this.binding.getCostumer().getPhone();

        Uri uri = Uri.parse(String.format(url, phone, message.getTextEncoded(EncodeType.URL)));

        this.startActivity(new Intent(Intent.ACTION_VIEW, uri));
    }
}
