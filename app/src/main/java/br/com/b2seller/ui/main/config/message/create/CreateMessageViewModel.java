package br.com.b2seller.ui.main.config.message.create;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Date;

import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.repository.MessageRepository;

public class CreateMessageViewModel extends ViewModel {
    MutableLiveData<Boolean> saved = new MutableLiveData<>(false);
    MutableLiveData<Integer> discount = new MutableLiveData<>();

    public void setDiscount(int discount) {
        Integer value = this.discount.getValue();

        if (value != null && discount == value) {
            this.discount.setValue(0);
        } else {
            this.discount.setValue(discount);
        }
    }

    public void save(MessageEntity message, String text, int discount) {
        try {
            if (message == null) {
                message = new MessageEntity();
            }

            message.setText(text);
            message.setCreatedAt(new Date());
            message.setDiscount(discount);

            if (message.getId() == 0) {
                MessageRepository.insert(message);
            } else {
                MessageRepository.update(message);
            }

            this.saved.setValue(true);
        } catch (Exception e) {
            this.saved.setValue(false);
        }
    }
}
