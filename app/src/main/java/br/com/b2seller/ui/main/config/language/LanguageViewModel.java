package br.com.b2seller.ui.main.config.language;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import br.com.b2seller.helper.LocaleHelper;

public class LanguageViewModel extends ViewModel {
    MutableLiveData<String> language;

    LanguageViewModel() {
        this.language = new MutableLiveData<>();
    }

    public void setLanguage(String language) {
        LocaleHelper.set(language);

        this.language.setValue(language);
    }
}
