package br.com.b2seller.ui.user.login;

import androidx.lifecycle.ViewModel;

import br.com.b2seller.R;
import br.com.b2seller.entity.UserEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppValidator;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.repository.UserRepository;

public class LoginViewModel extends ViewModel {
    public void login(String email, String password, RequestCallback<UserEntity> callback) {
        if (!AppValidator.email(email)) {
            callback.onFailure(new ResellerException(R.string.invalid_email));
        } else if (!AppValidator.password(password)) {
            callback.onFailure(new ResellerException(R.string.invalid_password));
        } else {
            Repository.exec(UserRepository.login(email, password), callback, UserEntity.class);
        }
    }
}