package br.com.b2seller.ui.main.costumer.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import br.com.b2seller.R;
import br.com.b2seller.databinding.AdapterOpportunitySmallBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.entity.ProductEntity;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.entity.SendEntity;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.base.BaseViewHolder;
import br.com.b2seller.identifier.BundleIdentifier;

public class OpportunitySmallAdapter extends RecyclerView.Adapter<OpportunitySmallAdapter.OpportunitySmallViewHolder> {
    private CostumerDetailsFragment fragment;
    private CostumerEntity costumer;
    private RuleEntity[] rules;

    public OpportunitySmallAdapter(CostumerDetailsFragment fragment, CostumerEntity costumer, RuleEntity[] rules) {
        this.fragment = fragment;
        this.costumer = costumer;
        this.rules = rules;
    }

    public void setRules(RuleEntity[] rules) {
        this.rules = rules;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OpportunitySmallViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterOpportunitySmallBinding binding = AdapterOpportunitySmallBinding.inflate(layoutInflater, parent, false);

        return new OpportunitySmallAdapter.OpportunitySmallViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OpportunitySmallViewHolder holder, int position) {
        holder.getBinding().setAdapter(this);
        holder.getBinding().setRule(this.rules[position]);
    }

    @Override
    public int getItemCount() {
        return this.rules.length;
    }

    public void clickView(View view, RuleEntity rule) {
        AppLog.events("client-details",
                new KeyValueEntity("costumerId", this.costumer.getId()),
                new KeyValueEntity("ruleAlias", rule.getAlias()));

        if (rule.getCount() > 0) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(BundleIdentifier.BUNDLE_PRODUCTS_TITLE, rule.getTitle());
            bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMER, this.costumer);
            bundle.putSerializable(BundleIdentifier.BUNDLE_PURCHASEDS, this.fragment.getPurchaseds());

            if (rule.getSends().length > 1) {
                bundle.putSerializable(BundleIdentifier.BUNDLE_SENDS, rule.getSends());

                Navigation.findNavController(view).navigate(R.id.action_costumer_sends, bundle);
            } else {
                SendEntity send = rule.getSends()[0];
                ProductEntity[] products = send.getData().getProducts().toArray(new ProductEntity[0]);
                ProductEntity[] recommendations = send.getData().getRecommendations().toArray(new ProductEntity[0]);

                bundle.putSerializable(BundleIdentifier.BUNDLE_PRODUCTS, products);
                bundle.putSerializable(BundleIdentifier.BUNDLE_RECOMMENDATIONS, recommendations);

                Navigation.findNavController(view).navigate(R.id.action_costumer_products, bundle);
            }
        }
    }

    static class OpportunitySmallViewHolder extends BaseViewHolder<AdapterOpportunitySmallBinding> {
        OpportunitySmallViewHolder(AdapterOpportunitySmallBinding binding) {
            super(binding);
        }
    }
}
