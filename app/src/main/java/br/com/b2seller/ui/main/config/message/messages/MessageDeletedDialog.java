package br.com.b2seller.ui.main.config.message.messages;

import android.app.Activity;
import android.app.Dialog;

import br.com.b2seller.R;
import br.com.b2seller.helper.base.BaseDialog;

class MessageDeletedDialog {
    static void show(Activity activity) {
        Dialog dialog = BaseDialog.create(activity, R.layout.dialog_message_deleted);
        dialog.findViewById(R.id.close).setOnClickListener(v -> dialog.cancel());

        dialog.show();
    }
}
