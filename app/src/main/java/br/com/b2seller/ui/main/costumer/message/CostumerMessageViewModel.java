package br.com.b2seller.ui.main.costumer.message;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.repository.MessageRepository;

public class CostumerMessageViewModel extends ViewModel {
    MutableLiveData<List<MessageEntity>> messages = new MutableLiveData<>();

    void fetchAll() {
        this.messages.postValue(MessageRepository.fetchAll());
    }
}
