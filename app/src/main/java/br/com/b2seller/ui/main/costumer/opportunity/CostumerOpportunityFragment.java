package br.com.b2seller.ui.main.costumer.opportunity;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import java.util.List;

import br.com.b2seller.databinding.FragmentCostumerOpportunityBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.helper.AppUtils;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.identifier.BundleIdentifier;

public class CostumerOpportunityFragment extends BaseFragment<FragmentCostumerOpportunityBinding> {
    private List<CostumerEntity> costumers;

    @Override
    public FragmentCostumerOpportunityBinding binding(LayoutInflater layoutInflater) {
        return FragmentCostumerOpportunityBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        if (bundle != null) {
            this.costumers = AppUtils.getList(bundle, BundleIdentifier.BUNDLE_COSTUMERS);

            if (this.costumers != null && this.costumers.size() > 0) {
                this.binding.setCostumer(this.costumers.get(0));
            }

            this.binding.setProductsTitle(bundle.getString(BundleIdentifier.BUNDLE_PRODUCTS_TITLE));
        }

        this.binding.setFragment(this);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        this.binding.rvDates.setAdapter(new CostumerOpportunityAdapter(this.costumers, this.binding.getProductsTitle()));
    }
}
