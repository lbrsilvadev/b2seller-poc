package br.com.b2seller.ui.main.config.message.create;

import android.app.Dialog;

import br.com.b2seller.R;
import br.com.b2seller.helper.base.BaseDialog;
import br.com.b2seller.helper.base.BaseFragment;

class MessageSavedDialog {
    @SuppressWarnings("all")
    static void show(BaseFragment fragment) {
        Dialog dialog = BaseDialog.create(fragment.activity(), R.layout.dialog_message_saved);
        dialog.findViewById(R.id.close).setOnClickListener(v -> {
            dialog.cancel();
            fragment.back();
        });

        dialog.show();
    }
}
