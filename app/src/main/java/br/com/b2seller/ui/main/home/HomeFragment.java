package br.com.b2seller.ui.main.home;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import br.com.b2seller.R;
import br.com.b2seller.databinding.FragmentHomeBinding;
import br.com.b2seller.entity.BannerEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppUser;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.helper.ui.Load;

public class HomeFragment extends BaseFragment<FragmentHomeBinding> {
    @Override
    public FragmentHomeBinding binding(LayoutInflater layoutInflater) {
        return FragmentHomeBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        this.binding.setViewModel(new HomeViewModel());
        this.binding.setFragment(this);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        this.binding.tvWelcome.setText(this.getString(R.string.hello_name, AppUser.get().getName()));

        Load.show(this.activity());

        this.binding.getViewModel().loadBanners(new RequestCallback<BannerEntity[]>() {
            @Override
            public void onSuccess(BannerEntity[] banners) {
                binding.getViewModel().count(count -> mainThread(() -> {
                    Load.dismiss();

                    binding.rvBanners.setAdapter(new BannerAdapter(banners, count));
                }));
            }

            @Override
            public void onFailure(ResellerException exception) {
                Load.dismiss();
            }
        });
    }
}
