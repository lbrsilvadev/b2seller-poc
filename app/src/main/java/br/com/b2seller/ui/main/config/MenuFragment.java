package br.com.b2seller.ui.main.config;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import br.com.b2seller.R;
import br.com.b2seller.databinding.FragmentMenuBinding;
import br.com.b2seller.helper.AppUser;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.ui.user.login.LoginActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends BaseFragment<FragmentMenuBinding> {
    @Override
    public FragmentMenuBinding binding(LayoutInflater layoutInflater) {
        return FragmentMenuBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        this.binding.setFragment(this);
        this.binding.setActionMessages(R.id.action_messages);
        this.binding.setActionLanguage(R.id.action_language);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
    }

    public void exit() {
        AppUser.remove();

        Intent intent = new Intent(this.activity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        this.startActivity(intent);
        this.activity().finish();
    }
}
