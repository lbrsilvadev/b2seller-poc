package br.com.b2seller.ui.main.opportunity;

import android.text.TextUtils;

import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.b2seller.entity.PurchasedEntity;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.entity.SendEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppRules;
import br.com.b2seller.helper.AppUser;
import br.com.b2seller.helper.Repository;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.repository.CostumerRepository;
import br.com.b2seller.repository.SendRepository;
import retrofit2.Call;

public class OpportunityViewModel extends ViewModel {
    public void loadRules(RequestCallback<RuleEntity[]> callback) {
        this.loadRules(null, callback);
    }

    public void loadRules(String id, RequestCallback<RuleEntity[]> callback) {
        new Thread(() -> {
            try {
                callback.onSuccess(fetchCount(id));
            } catch (ResellerException exception) {
                callback.onFailure(exception);
            }
        }).start();
    }

    public void loadPurchaseds(RequestCallback<PurchasedEntity[]> callback) {
        Call<JsonObject> call = CostumerRepository.purchased(AppUser.get().getId());
        Repository.exec(call, callback, PurchasedEntity[].class);
    }

    private RuleEntity[] fetchCount(String id) throws ResellerException {
        RuleEntity[] rules = AppRules.get();

        Call<JsonObject> callSends = SendRepository.fetchAll(AppUser.get().getId());
        JsonObject sends = Repository.execSync(callSends, JsonObject.class);

        for (RuleEntity rule : rules) {
            if (sends.has(String.valueOf(rule.getId()))) {
                rule.setCount(sends.get(String.valueOf(rule.getId())).getAsInt());
            }
        }

        Arrays.sort(rules, (rule1, rule2) -> Integer.compare(rule2.getCount(), rule1.getCount()));

        return this.filterCount(rules, id);
    }

    private RuleEntity[] filterCount(RuleEntity[] rules, String id) {
        List<RuleEntity> list = new ArrayList<>();

        for (RuleEntity rule : rules) {
            if (!TextUtils.isEmpty(id) && rule.getCount() > 0) {
                this.setupSends(rule, id);
            }

            if (rule.getCount() > 0) {
                list.add(rule);
            }
        }

        return list.toArray(new RuleEntity[0]);
    }

    private void setupSends(RuleEntity rule, String id) {
        try {
            Call<JsonObject> callSends = SendRepository.fetchCostumers(AppUser.get().getId(), rule.getId());
            rule.setSends(Repository.execSync(callSends, SendEntity[].class), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
