package br.com.b2seller.ui.main.config.message.messages;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import java.util.List;

import br.com.b2seller.R;
import br.com.b2seller.databinding.AdapterMessageBinding;
import br.com.b2seller.entity.MessageEntity;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.helper.base.BaseViewHolder;
import br.com.b2seller.identifier.BundleIdentifier;
import br.com.b2seller.ui.main.config.message.messages.DeleteMessageDialog.Callback;
import br.com.b2seller.ui.main.costumer.message.CostumerMessageFragment;

@SuppressWarnings("all")
public class MessageAdapter extends Adapter<MessageAdapter.MessageViewHolder> {
    private BaseFragment fragment;
    private List<MessageEntity> messages;
    private final boolean editable;

    public MessageAdapter(BaseFragment fragment, List<MessageEntity> messages, boolean editable) {
        this.fragment = fragment;
        this.messages = messages;
        this.editable = editable;
    }

    public void setMessages(List<MessageEntity> messages) {
        this.messages = messages;

        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterMessageBinding binding = AdapterMessageBinding.inflate(layoutInflater, parent, false);

        return new MessageViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        holder.getBinding().setAdapter(this);
        holder.getBinding().setEditable(this.editable);
        holder.getBinding().setMessage(this.messages.get(position));
    }

    @Override
    public int getItemCount() {
        return this.messages.size();
    }

    public void edit(MessageEntity message) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleIdentifier.BUNDLE_MESSAGE, message);

        this.fragment.navigate(R.id.action_create_message, bundle);
    }

    public void delete(MessageEntity message) {
        DeleteMessageDialog.show(new Callback() {
            @Override
            public Context context() {
                return fragment.activity();
            }

            @Override
            public void onDelete() {
                if (fragment instanceof MessagesFragment) {
                    ((MessagesFragment) fragment).delete(message);
                }
            }
        });
    }

    public void send(MessageEntity message) {
        if (this.fragment instanceof CostumerMessageFragment) {
            ((CostumerMessageFragment) this.fragment).send(message);
        }
    }

    static class MessageViewHolder extends BaseViewHolder<AdapterMessageBinding> {
        MessageViewHolder(AdapterMessageBinding binding) {
            super(binding);
        }
    }
}
