package br.com.b2seller.ui.user.forgotpassword;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

import br.com.b2seller.R;
import br.com.b2seller.helper.base.BaseDialog;

public class ForgotPasswordDialog {
    static void show(Activity activity) {
        Dialog dialog = BaseDialog.create(activity, R.layout.dialog_forgot_password);
        dialog.findViewById(R.id.close).setOnClickListener(v ->
                ForgotPasswordDialog.animationHide(activity, dialog));

        ForgotPasswordDialog.animationShow(activity, dialog);

        dialog.show();
    }

    private static void animationShow(Activity activity, Dialog dialog) {
        View view = dialog.findViewById(R.id.content);
        Animation animationBottomUp = AnimationUtils.loadAnimation(activity, R.anim.bottom_up);
        view.startAnimation(animationBottomUp);
        view.setVisibility(View.VISIBLE);
    }

    private static void animationHide(Activity activity, Dialog dialog) {
        View view = dialog.findViewById(R.id.content);

        if (Integer.parseInt(view.getTag().toString()) == 0) {
            view.setTag(1);

            Animation animationBottomDown = AnimationUtils.loadAnimation(activity, R.anim.bottom_down);
            animationBottomDown.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                    dialog.dismiss();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            view.startAnimation(animationBottomDown);
        }
    }
}
