package br.com.b2seller.ui.user.forgotpassword;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.gson.JsonObject;

import br.com.b2seller.databinding.ActivityForgotPasswordBinding;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.helper.base.BaseActivity;
import br.com.b2seller.helper.ui.Alert;
import br.com.b2seller.helper.ui.Load;

public class ForgotPasswordActivity extends BaseActivity<ActivityForgotPasswordBinding> {
    @Override
    public ActivityForgotPasswordBinding binding(LayoutInflater layoutInflater) {
        return ActivityForgotPasswordBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        this.binding.setActivity(this);
        this.binding.setViewModel(new ForgotPasswordViewModel());
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
    }

    public void clickBack(View view) {
        this.finish();
    }

    public void recover(String email) {
        Load.show(this);

        this.binding.getViewModel().recover(email, new RequestCallback<JsonObject>() {
            @Override
            public void onSuccess(JsonObject response) {
                Load.dismiss();

                ForgotPasswordDialog.show(ForgotPasswordActivity.this);
            }

            @Override
            public void onFailure(ResellerException exception) {
                Load.dismiss();

                Alert.show(ForgotPasswordActivity.this, exception.getMessage());
            }
        });
    }
}
