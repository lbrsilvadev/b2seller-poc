package br.com.b2seller.ui.main.opportunity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import br.com.b2seller.R;
import br.com.b2seller.databinding.AdapterOpportunityBinding;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.helper.base.BaseViewHolder;
import br.com.b2seller.identifier.BundleIdentifier;

public class OpportunityAdapter extends RecyclerView.Adapter<OpportunityAdapter.OpportunityViewHolder> {
    private RuleEntity[] rules;

    public OpportunityAdapter(RuleEntity[] rules) {
        this.rules = rules;
    }

    public void setRules(RuleEntity[] rules) {
        this.rules = rules;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OpportunityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AdapterOpportunityBinding binding = AdapterOpportunityBinding.inflate(layoutInflater, parent, false);

        return new OpportunityAdapter.OpportunityViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OpportunityViewHolder holder, int position) {
        holder.getBinding().setAdapter(this);
        holder.getBinding().setRule(this.rules[position]);
    }

    @Override
    public int getItemCount() {
        return this.rules.length;
    }

    public void clickView(View view, RuleEntity rule) {
        if (rule.getCount() > 0) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(BundleIdentifier.BUNDLE_RULE, rule);

            Navigation.findNavController(view).navigate(R.id.action_opportunity_clients, bundle);
        }
    }

    static class OpportunityViewHolder extends BaseViewHolder<AdapterOpportunityBinding> {
        OpportunityViewHolder(AdapterOpportunityBinding binding) {
            super(binding);
        }
    }
}
