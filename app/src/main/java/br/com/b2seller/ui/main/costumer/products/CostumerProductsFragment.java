package br.com.b2seller.ui.main.costumer.products;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.List;

import br.com.b2seller.R;
import br.com.b2seller.databinding.FragmentCostumerProductsBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.entity.ProductEntity;
import br.com.b2seller.entity.PurchasedEntity;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.AppUtils;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.identifier.BundleIdentifier;

public class CostumerProductsFragment extends BaseFragment<FragmentCostumerProductsBinding> {
    private List<ProductEntity> products;
    private List<ProductEntity> recommendations;
    private List<PurchasedEntity> purchaseds;

    @Override
    public FragmentCostumerProductsBinding binding(LayoutInflater layoutInflater) {
        return FragmentCostumerProductsBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        if (bundle != null) {
            this.binding.setCostumer((CostumerEntity) bundle.getSerializable(BundleIdentifier.BUNDLE_COSTUMER));
            this.binding.setProductsTitle(bundle.getString(BundleIdentifier.BUNDLE_PRODUCTS_TITLE, ""));
            this.products = AppUtils.getList(bundle, BundleIdentifier.BUNDLE_PRODUCTS);
            this.purchaseds = AppUtils.getList(bundle, BundleIdentifier.BUNDLE_PURCHASEDS);
            this.recommendations = AppUtils.getList(bundle, BundleIdentifier.BUNDLE_RECOMMENDATIONS);
        }

        this.binding.setFragment(this);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        this.binding.rvProducts.addItemDecoration(AppUtils.divider(this.activity()));
        this.binding.rvProducts.setNestedScrollingEnabled(false);
        this.binding.rvProducts.setAdapter(new ProductLinearAdapter(this.products).setPurchaseds(this.purchaseds));

        if (this.recommendations != null && this.recommendations.size() > 0) {
            this.binding.recommendations.setVisibility(View.VISIBLE);

            this.binding.rvRecommendedProducts.addItemDecoration(AppUtils.divider(this.activity()));
            this.binding.rvRecommendedProducts.setNestedScrollingEnabled(false);
            this.binding.rvRecommendedProducts.setAdapter(new ProductLinearAdapter(this.recommendations));
        } else {
            this.binding.recommendations.setVisibility(View.GONE);
        }
    }

    public void clickContact(CostumerEntity costumer) {
        AppLog.events("contact",
                new KeyValueEntity("costumerId", costumer.getId()),
                new KeyValueEntity("from", "costumer-products"));

        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMER, costumer);

        this.navigate(R.id.action_costumer_contact, bundle);
    }
}
