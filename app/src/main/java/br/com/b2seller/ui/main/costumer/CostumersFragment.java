package br.com.b2seller.ui.main.costumer;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.List;

import br.com.b2seller.databinding.FragmentCostumersBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.entity.SendEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.RequestCallback;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.helper.ui.Alert;
import br.com.b2seller.helper.ui.Load;
import br.com.b2seller.identifier.BundleIdentifier;

/**
 * A simple {@link Fragment} subclass.
 */
public class CostumersFragment extends BaseFragment<FragmentCostumersBinding> implements RequestCallback<Object> {
    private RuleEntity rule;

    @Override
    public FragmentCostumersBinding binding(LayoutInflater layoutInflater) {
        return FragmentCostumersBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        this.binding.setViewModel(new CostumersViewModel());
        this.binding.setShowActionBar(bundle != null);
        this.binding.setFragment(this);

        if (bundle != null) {
            this.rule = (RuleEntity) bundle.getSerializable(BundleIdentifier.BUNDLE_RULE);

            if (this.rule != null) {
                AppLog.events("opportunity", new KeyValueEntity("ruleAlias", this.rule.getAlias()));
            }
        } else {
            AppLog.events("clients");
        }
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        if (this.rule != null) {
            this.binding.setTitle(this.rule.getTitle());
        }

        Load.show(this.activity());

        this.binding.getViewModel().load(this.rule, this);
        this.binding.getViewModel().liveDataCostumers.observe(this.getViewLifecycleOwner(), costumers -> {
            Load.dismiss();

            if (this.binding.rvClients.getAdapter() != null) {
                ((CostumerAdapter) this.binding.rvClients.getAdapter()).setCostumers(costumers);
            } else {
                this.binding.rvClients.setAdapter(new CostumerAdapter(costumers, this.getCostumers(), this.binding.getTitle(), this.rule));
            }
        });
    }

    @Override
    public void onSuccess(Object response) {
        if (response instanceof CostumerEntity[]) {
            this.binding.getViewModel().parseCostumers((CostumerEntity[]) response);
        } else if (response instanceof SendEntity[]) {
            this.binding.getViewModel().parseSends((SendEntity[]) response);
        }
    }

    @Override
    public void onFailure(ResellerException exception) {
        Load.dismiss();

        Alert.show(activity(), exception.getMessage());
    }

    public List<CostumerEntity> getCostumers() {
        return this.binding.getViewModel().getCostumers();
    }
}