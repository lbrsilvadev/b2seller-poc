package br.com.b2seller.ui.main.config.message.messages;

import android.app.Dialog;
import android.content.Context;

import br.com.b2seller.R;
import br.com.b2seller.helper.base.BaseDialog;

class DeleteMessageDialog {
    static void show(Callback callback) {
        Dialog dialog = BaseDialog.create(callback.context(), R.layout.dialog_delete_message);
        dialog.findViewById(R.id.delete).setOnClickListener(v -> {
            callback.onDelete();
            dialog.cancel();
        });
        dialog.findViewById(R.id.cancel).setOnClickListener(v -> dialog.cancel());

        dialog.show();
    }

    public interface Callback {
        Context context();

        void onDelete();
    }
}
