package br.com.b2seller.ui.main.costumer.contact;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import br.com.b2seller.R;
import br.com.b2seller.databinding.FragmentCostumerContactBinding;
import br.com.b2seller.entity.CostumerEntity;
import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.helper.AppLog;
import br.com.b2seller.helper.base.BaseFragment;
import br.com.b2seller.identifier.BundleIdentifier;
import br.com.b2seller.identifier.RequestIdentifier;
import br.com.b2seller.type.MessageType;

public class CostumerContactFragment extends BaseFragment<FragmentCostumerContactBinding> {
    @Override
    public FragmentCostumerContactBinding binding(LayoutInflater layoutInflater) {
        return FragmentCostumerContactBinding.inflate(layoutInflater);
    }

    @Override
    protected void setup(@Nullable Bundle bundle) {
        super.setup(bundle);

        if (bundle != null) {
            this.binding.setCostumer((CostumerEntity) bundle.getSerializable(BundleIdentifier.BUNDLE_COSTUMER));
        }

        this.binding.setFragment(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            this.call();
        }
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
    }

    public void whatsapp(CostumerEntity costumer) {
        AppLog.events("contact",
                new KeyValueEntity("costumerId", costumer.getId()),
                new KeyValueEntity("type", "whatsapp"));

        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMER, costumer);
        bundle.putSerializable(BundleIdentifier.BUNDLE_MESSAGE_TYPE, MessageType.WHATSAPP);

        this.navigate(R.id.action_costumer_message, bundle);
    }

    public void email(CostumerEntity costumer) {
        AppLog.events("contact",
                new KeyValueEntity("costumerId", costumer.getId()),
                new KeyValueEntity("type", "email"));

        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleIdentifier.BUNDLE_COSTUMER, costumer);
        bundle.putSerializable(BundleIdentifier.BUNDLE_MESSAGE_TYPE, MessageType.EMAIL);

        this.navigate(R.id.action_costumer_message, bundle);
    }

    public void requestCall() {
        if (ActivityCompat.checkSelfPermission(this.activity(),
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            this.call();
        } else {
            String[] permissions = new String[] { Manifest.permission.CALL_PHONE };

            this.requestPermissions(permissions, RequestIdentifier.REQUEST_CALL);
        }
    }

    private void call() {
        AppLog.events("contact",
                new KeyValueEntity("costumerId", this.binding.getCostumer().getId()),
                new KeyValueEntity("type", "call"));

        Uri uri = Uri.fromParts("tel", this.binding.getCostumer().getPhone(), null);
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        startActivity(intent);
    }
}
