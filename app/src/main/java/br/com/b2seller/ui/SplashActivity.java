package br.com.b2seller.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import br.com.b2seller.BuildConfig;
import br.com.b2seller.databinding.ActivitySplashBinding;
import br.com.b2seller.helper.AppUser;
import br.com.b2seller.helper.base.BaseActivity;
import br.com.b2seller.ui.main.MainActivity;
import br.com.b2seller.ui.user.login.LoginActivity;

public class SplashActivity extends BaseActivity<ActivitySplashBinding> implements Runnable {
    @Override
    public ActivitySplashBinding binding(LayoutInflater layoutInflater) {
        return ActivitySplashBinding.inflate(layoutInflater);
    }

    @Override
    public void onViewCreated(@Nullable Bundle bundle) {
        new Thread(this).start();
    }

    @Override
    public void run() {
        SystemClock.sleep(BuildConfig.SPLASH_TIME * 1000);

        Intent intent = new Intent(this, AppUser.has() ? MainActivity.class : LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        this.startActivity(intent);
        this.finish();
    }
}
