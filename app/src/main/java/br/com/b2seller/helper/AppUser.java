package br.com.b2seller.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import br.com.b2seller.ResellerApplication;
import br.com.b2seller.entity.UserEntity;
import br.com.b2seller.repository.UserRepository;
import br.com.b2seller.repository.config.database.AppDatabase;
import br.com.b2seller.repository.config.network.JWT;

import static android.content.Context.MODE_PRIVATE;
import static br.com.b2seller.identifier.SharedIdentifier.SHARED_USER;
import static br.com.b2seller.identifier.SharedIdentifier.USER;

public class AppUser {
    public static void save(UserEntity user) {
        Context context = ResellerApplication.context();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_USER, MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(USER, JWT.encode(user));
        editor.apply();
    }

    public static UserEntity get() {
        Context context = ResellerApplication.context();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_USER, MODE_PRIVATE);
        String jwt = sharedPreferences.getString(USER, null);

        return JWT.decode(jwt, UserEntity.class);
    }

    public static void remove() {
        Repository.exec(UserRepository.clear(AppUser.get().getId()));
        AppDatabase.clear();

        Context context = ResellerApplication.context();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_USER, MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.remove(USER);
        editor.apply();
    }

    public static boolean has() {
        Context context = ResellerApplication.context();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_USER, MODE_PRIVATE);

        return sharedPreferences.contains(USER);
    }
}
