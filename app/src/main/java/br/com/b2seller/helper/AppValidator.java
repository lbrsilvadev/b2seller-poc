package br.com.b2seller.helper;

import android.text.TextUtils;
import android.util.Patterns;

public class AppValidator {
    public static boolean email(String value) {
        if (TextUtils.isEmpty(value)) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(value).matches();
        }
    }

    public static boolean password(String value) {
        return !TextUtils.isEmpty(value) && value.length() > 4;
    }
}
