package br.com.b2seller.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;

import androidx.preference.PreferenceManager;

import java.util.Locale;

import br.com.b2seller.ResellerApplication;

public class LocaleHelper {
    private static final String SELECTED_LANGUAGE = "Locale.Helper.Selected.Language";

    public static void initialize() {
        LocaleHelper.set(LocaleHelper.getLang());
    }

    public static String getLang() {
        String lang;

        if (LocaleHelper.get().isEmpty()) {
            lang = LocaleHelper.persistedData(Locale.getDefault().getLanguage());
        } else {
            lang = LocaleHelper.get();
        }

        return lang;
    }

    public static Resources getResources(String language) {
        Context context = ResellerApplication.context();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return resources;
    }

    public static void set(String language) {
        LocaleHelper.persist(language);
        LocaleHelper.updateResources(language);
    }

    public static String get() {
        return LocaleHelper.persistedData(Locale.getDefault().getLanguage());
    }

    private static String persistedData(String defaultLanguage) {
        Context context = ResellerApplication.context();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(SELECTED_LANGUAGE, defaultLanguage);
    }

    private static void persist(String language) {
        Context context = ResellerApplication.context();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(SELECTED_LANGUAGE, language);
        editor.apply();
    }

    private static void updateResources(String language) {
        Context context = ResellerApplication.context();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }
}
