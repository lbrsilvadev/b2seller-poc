package br.com.b2seller.helper.base;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.viewbinding.ViewBinding;

import org.jetbrains.annotations.NotNull;

import br.com.b2seller.helper.ui.SoftKeyboard;

public abstract class BaseFragment<T> extends Fragment implements BaseProtocol<T> {
    // Variables
    protected T binding;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {
        this.binding = this.binding(inflater);

        return ((ViewBinding) this.binding).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);

        this.setup(this.getArguments());
        this.onViewCreated(this.getArguments());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        this.binding = null;
    }

    public Activity activity() {
        return this.requireActivity();
    }

    public void mainThread(Runnable runnable) {
        try {
            this.activity().runOnUiThread(runnable);
        } catch (Exception e) {
            Log.d("Base Fragment", "Error on execute on UI Thread");
        }
    }

    public void navigate(@IdRes int resId) {
        this.navigate(resId, null);
    }

    public void navigate(@IdRes int resId, @Nullable Bundle bundle) {
        if (this.getView() != null) {
            Navigation.findNavController(this.getView()).navigate(resId, bundle);
        }
    }

    public void back() {
        SoftKeyboard.dismiss(this.activity());

        this.activity().onBackPressed();
    }

    // All view settings must be made in this method which must be override in the view that extends this class
    protected void setup(@Nullable Bundle bundle) {
        // Dummy
    }
}
