package br.com.b2seller.helper.base;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

public interface BaseProtocol<T> {
    // Methods
    T binding(LayoutInflater layoutInflater);

    void onViewCreated(@Nullable Bundle bundle);
}
