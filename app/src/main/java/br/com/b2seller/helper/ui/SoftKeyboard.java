package br.com.b2seller.helper.ui;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class SoftKeyboard {
    public static void dismiss(Fragment fragment) {
        SoftKeyboard.dismiss(fragment.requireActivity());
    }

    public static void dismiss(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);

            if (inputMethodManager != null && activity.getCurrentFocus() != null) {
                IBinder token = activity.getCurrentFocus().getWindowToken();
                inputMethodManager.hideSoftInputFromWindow(token, 0);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void open(Fragment fragment, EditText editText) {
        SoftKeyboard.dismiss(fragment.requireActivity());
    }

    public static void open(Activity activity, EditText editText) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }
}
