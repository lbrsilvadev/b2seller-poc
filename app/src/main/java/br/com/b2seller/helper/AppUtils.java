package br.com.b2seller.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import br.com.b2seller.R;
import br.com.b2seller.ResellerApplication;
import br.com.b2seller.helper.ui.MyDividerItemDecoration;

public class AppUtils {
    public static String replaceLast(String string, String substring, String replacement) {
        int index = string.lastIndexOf(substring);

        if (index == -1) {
            return string;
        }

        return string.substring(0, index) + replacement + string.substring(index + substring.length());
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> getList(Bundle bundle, String key) {
        T[] serializable = (T[]) bundle.getSerializable(key);

        return serializable != null ? Arrays.asList(serializable) : null;
    }

    public static MyDividerItemDecoration divider(Context context) {
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.divider_gray);

        return new MyDividerItemDecoration(drawable);
    }

    public static <T> T parseJson(JsonObject json, Class<T> clazz) {
        JsonElement data = json.get("data");

        if (data.isJsonArray()) {
            JsonArray array = json.getAsJsonArray("data");
            JsonArray newJsonArray = new JsonArray();

            for (JsonElement element : array) {
                if (element.isJsonObject()) {
                    JsonObject object = element.getAsJsonObject();

                    if (object.has("_source")) {
                        newJsonArray.add(object.get("_source"));
                    } else {
                        newJsonArray.add(object);
                    }
                }
            }

            return new Gson().fromJson(newJsonArray, clazz);
        } else {
            JsonObject object = data.getAsJsonObject();

            if (object.has("_source")) {
                return new Gson().fromJson(object.get("_source"), clazz);
            }

            return new Gson().fromJson(data, clazz);
        }
    }

    public static String md5(String value) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(value.getBytes());
            byte[] messageDigest = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);

                while (h.length() < 2) {
                    h = "0".concat(h);
                }

                hexString.append(h);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String dateFormat(String date, String oldFormat, String newFormat) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(oldFormat, Locale.US);

            if (oldFormat.contains("'Z'")) {
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
            }

            Date oldDate = format.parse(date);

            if (oldDate != null) {
                return new SimpleDateFormat(newFormat, Locale.getDefault()).format(oldDate);
            }

            return date;
        } catch (Exception e) {
            return date;
        }
    }

    public static String money(String value) {
        return AppUtils.money(Double.parseDouble(value));
    }

    public static String money(double value) {
        return NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(value);
    }

    public static boolean isDouble(String value) {
        try {
            Double.valueOf(value);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String string(String value) {
        try {
            Context context = ResellerApplication.context();
            Resources resources = context.getResources();
            int identifier = resources.getIdentifier(value, "string", context.getPackageName());
            String string = resources.getString(identifier);

            if (TextUtils.isEmpty(string)) {
                return value;
            }

            return string;
        } catch (Exception e) {
            return value;
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public static Drawable mipmap(String value) {
        Context context = ResellerApplication.context();
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier(value, "mipmap", context.getPackageName());

        return resources.getDrawable(identifier);
    }
}
