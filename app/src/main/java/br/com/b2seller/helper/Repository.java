package br.com.b2seller.helper;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.repository.config.network.JWT;
import br.com.b2seller.repository.config.network.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    public static <T> T network(Class<T> clazz) {
        return ServiceGenerator.get(clazz);
    }

    public static <T> T network(String baseUrl, Class<T> clazz) {
        return ServiceGenerator.get(baseUrl, clazz);
    }

    public static String header(JsonObject jsonObject) {
        return JWT.encode(jsonObject);
    }

    public static <T> void exec(Call<JsonObject> call) {
        Repository.exec(call, null, null);
    }

    public static <T> void exec(Call<JsonObject> call, RequestCallback<T> callback, Class<T> clazz) {
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                if (callback != null) {
                    try {
                        callback.onSuccess(Repository.onResponse(response, clazz));
                    } catch (ResellerException exception) {
                        callback.onFailure(exception);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable throwable) {
                if (callback != null) {
                    callback.onFailure(new ResellerException(throwable.getMessage()));
                }
            }
        });
    }

    public static <T> T execSync(Call<JsonObject> call, Class<T> clazz) throws ResellerException {
        try {
            return Repository.onResponse(call.execute(), clazz);
        } catch (Exception e) {
            throw new ResellerException(e.getMessage());
        }
    }

    private static <T> T onResponse(Response<JsonObject> response, Class<T> clazz) throws ResellerException {
        if (response.isSuccessful() && response.body() != null) {
            return AppUtils.parseJson(response.body(), clazz);
        } else if (response.errorBody() != null) {
            String errorBody;

            try {
                errorBody = response.errorBody().string();
            } catch (Exception e) {
                errorBody = null;
            }

            if (!TextUtils.isEmpty(errorBody)) {
                JsonObject json = new Gson().fromJson(errorBody, JsonObject.class);

                if (json.has("message")) {
                    String message = json.get("message").getAsString();

                    throw new ResellerException(message);
                }
            }
        }

        throw new ResellerException();
    }
}
