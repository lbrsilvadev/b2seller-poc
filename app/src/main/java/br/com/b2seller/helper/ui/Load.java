package br.com.b2seller.helper.ui;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;

import br.com.b2seller.R;

public class Load {
    private static Dialog mLoading = null;

    public static void show(Activity activity) {
        try {
            if (activity.isFinishing()) {
                Load.dismiss();

                return;
            }

            Load.dismiss();

            SoftKeyboard.dismiss(activity);

            if (!Load.isShowing() && !activity.isFinishing()) {
                Load.mLoading = new Dialog(activity, R.style.TransparentDialog);
                Load.mLoading.setContentView(R.layout.view_load);
                Load.mLoading.setCancelable(false);
                Load.mLoading.show();
            }
        } catch (Exception e) {
            Load.dismiss();
        }
    }

    public static boolean isShowing() {
        return Load.mLoading != null && Load.mLoading.isShowing();
    }

    public static void dismiss() {
        try {
            if (Load.mLoading != null) {
                Load.mLoading.dismiss();
            }
        } catch (Exception exception) {
            Log.e("Load", "Error on dismiss load");
        } finally {
            Load.mLoading = null;
        }
    }
}
