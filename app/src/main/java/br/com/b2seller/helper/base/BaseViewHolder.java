package br.com.b2seller.helper.base;

import android.app.Activity;
import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {
    protected T binding;

    public BaseViewHolder(T binding) {
        super(((ViewBinding) binding).getRoot());

        this.binding = binding;
    }

    public T getBinding() {
        return binding;
    }

    public Activity activity() {
        return (Activity) this.itemView.getContext();
    }

    public Context context() {
        return this.itemView.getContext();
    }
}
