package br.com.b2seller.helper;

import java.util.Collections;
import java.util.List;

import br.com.b2seller.entity.SendEntity;

public class ArraySend {
    public static List<SendEntity> orderByDate(List<SendEntity> sends) {
        Collections.sort(sends, (s1, s2) -> s2.getDate().compareTo(s1.getDate()));

        return sends;
    }
}
