package br.com.b2seller.helper.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import java.util.Locale;

import br.com.b2seller.helper.LocaleHelper;
import br.com.b2seller.helper.MyContextWrapper;

public abstract class BaseActivity<T> extends AppCompatActivity implements BaseProtocol<T> {
    // Variables
    protected T binding;

    @Override
    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);

        LayoutInflater inflater = LayoutInflater.from(this);

        this.binding = this.binding(inflater);

        this.setContentView(((ViewBinding) this.binding).getRoot());

        this.setup(this.getIntent().getExtras());
        this.onViewCreated(this.getIntent().getExtras());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale newLocale = new Locale(LocaleHelper.get());

        super.attachBaseContext(MyContextWrapper.wrap(newBase, newLocale));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.binding = null;
    }

    // All view settings must be made in this method which must be override in the view that extends this class
    protected void setup(@Nullable Bundle bundle) {
        // Dummy
    }
}
