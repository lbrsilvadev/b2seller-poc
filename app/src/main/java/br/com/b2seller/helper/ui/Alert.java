package br.com.b2seller.helper.ui;

import android.app.Activity;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import br.com.b2seller.helper.AppUtils;

public class Alert {
    public static void show(Fragment fragment, int message) {
        Alert.show(fragment.requireActivity(), message);
    }

    public static void show(Activity activity, int message) {
        Alert.show(activity, 0, message, null);
    }

    public static void show(Fragment fragment, int title, int message) {
        Alert.show(fragment.requireActivity(), title, message);
    }

    public static void show(Activity activity, int title, int message) {
        Alert.show(activity, title, message, null);
    }

    public static void show(Fragment fragment, int title, int message, OnOkClick onClick) {
        Alert.show(fragment.requireActivity(), title, message, onClick);
    }

    public static void show(Activity activity, int title, int message, OnOkClick onClick) {
        String sTitle = message != 0 ? activity.getString(title) : null;
        String sMessage = message != 0 ? activity.getString(message) : null;

        Alert.show(activity, sTitle, sMessage, onClick);
    }

    public static void show(Fragment fragment, String message) {
        Alert.show(fragment.requireActivity(), message);
    }

    public static void show(Activity activity, String message) {
        Alert.show(activity, null, message, null);
    }

    public static void show(Fragment fragment, String title, String message) {
        Alert.show(fragment.requireActivity(), title, message);
    }

    public static void show(Activity activity, String title, String message) {
        Alert.show(activity, title, message, null);
    }

    public static void show(Fragment fragment, String title, String message, OnOkClick onClick) {
        Alert.show(fragment.requireActivity(), title, message, onClick);
    }

    public static void show(Activity activity, String title, String message, OnOkClick onClick) {
        try {
            Load.dismiss();

            activity.runOnUiThread(() -> {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                alertDialogBuilder.setTitle(title);

                if (!message.contains(" ")) {
                    String identifier = message.toLowerCase();

                    alertDialogBuilder.setMessage(AppUtils.string(identifier));
                } else {
                    alertDialogBuilder.setMessage(message);
                }

                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    dialog.dismiss();

                    if (onClick != null) {
                        onClick.click();
                    }
                });
                alertDialogBuilder.create().show();
            });
        } catch (Exception e) {
            Log.d("Alert", "Error on execute on UI Thread");
        }
    }

    public interface OnOkClick {
        void click();
    }
}
