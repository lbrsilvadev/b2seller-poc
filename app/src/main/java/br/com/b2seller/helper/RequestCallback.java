package br.com.b2seller.helper;

import br.com.b2seller.exception.ResellerException;

public interface RequestCallback<T> {
    void onSuccess(T response);

    void onFailure(ResellerException exception);
}
