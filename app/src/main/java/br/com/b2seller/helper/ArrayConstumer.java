package br.com.b2seller.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.b2seller.entity.CostumerEntity;

public class ArrayConstumer {
    public static List<CostumerEntity> unique(List<CostumerEntity> costumers) {
        List<CostumerEntity> list = new ArrayList<>();

        for (CostumerEntity costumer : costumers) {
            if (!ArrayConstumer.contains(list, costumer)) {
                list.add(costumer);
            }
        }

        return list;
    }

    public static boolean contains(List<CostumerEntity> costumers, CostumerEntity costumer) {
        for (CostumerEntity entity : costumers) {
            if (entity.getMd5Id().equals(costumer.getMd5Id())) {
                return true;
            }
        }

        return false;
    }

    public static List<CostumerEntity> get(List<CostumerEntity> costumers, String md5) {
        List<CostumerEntity> list = new ArrayList<>();

        for (CostumerEntity costumer : costumers) {
            if (costumer.getMd5Id().equals(md5)) {
                list.add(costumer);
            }
        }

        return list;
    }

    public static List<CostumerEntity> orderByDate(List<CostumerEntity> costumers) {
        Collections.sort(costumers, (c1, c2) -> c2.getDate().compareTo(c1.getDate()));

        return costumers;
    }
}
