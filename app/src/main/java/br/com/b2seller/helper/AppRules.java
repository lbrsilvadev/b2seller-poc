package br.com.b2seller.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import br.com.b2seller.ResellerApplication;
import br.com.b2seller.entity.RuleEntity;
import br.com.b2seller.exception.ResellerException;
import br.com.b2seller.repository.RuleRepository;
import br.com.b2seller.repository.config.network.JWT;

import static android.content.Context.MODE_PRIVATE;
import static br.com.b2seller.identifier.SharedIdentifier.RULES;
import static br.com.b2seller.identifier.SharedIdentifier.SHARED_RULLES;

public class AppRules {
    public static void save(RuleEntity[] rules) {
        Context context = ResellerApplication.context();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_RULLES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(RULES, JWT.encode(AppRules.filter(rules)));
        editor.apply();
    }

    public static RuleEntity[] get() {
        Context context = ResellerApplication.context();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_RULLES, MODE_PRIVATE);
        String jwt = sharedPreferences.getString(RULES, null);

        return JWT.decode(jwt, RuleEntity[].class);
    }

    public static boolean has() {
        Context context = ResellerApplication.context();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_RULLES, MODE_PRIVATE);

        return sharedPreferences.contains(RULES);
    }

    public static void load() {
        if (!AppRules.has()) {
            Repository.exec(RuleRepository.fetchAll(), new RequestCallback<RuleEntity[]>() {
                @Override
                public void onSuccess(RuleEntity[] rules) {
                    AppRules.save(rules);
                }

                @Override
                public void onFailure(ResellerException exception) {
                }
            }, RuleEntity[].class);
        }
    }

    public static RuleEntity[] filter(RuleEntity[] rules) {
        List<String> whiteList = Arrays.asList(
                "cart-abandonment",
                "regain",
                "navigation-daily",
                "navigation-weekly",
                "navigation-monthly",
                "decided",
                "birthday",
                "recurrence",
                "wishlist",
                "trend",
                "after-buy",
                "warn-me"
        );

        List<RuleEntity> list = new ArrayList<>();

        for (RuleEntity rule : rules) {
            if (whiteList.contains(rule.getAlias())) {
                list.add(rule);
            }
        }

        Collections.sort(list, (rule1, rule2) -> rule1.getAlias().compareTo(rule2.getAlias()));

        return list.toArray(new RuleEntity[0]);
    }

    public static boolean hideCount(String alias) {
        List<String> whiteList = Arrays.asList(
                "birthday",
                "regain"
        );

        return whiteList.contains(alias);
    }

    public static Drawable icon(String alias) {
        alias = alias
                .replace("-", "_")
                .replace("_daily", "")
                .replace("_weekly", "")
                .replace("_monthly", "");

        return AppUtils.mipmap(alias);
    }

    public static String text(String alias, boolean title) {
        alias = alias.replace("-", "_").concat("_button");

        if (title) {
            return AppUtils.string(alias).replace("\n", " ");
        } else {
            return AppUtils.string(alias);
        }
    }
}
