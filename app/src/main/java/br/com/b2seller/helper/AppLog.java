package br.com.b2seller.helper;

import com.google.gson.JsonObject;

import br.com.b2seller.entity.KeyValueEntity;
import br.com.b2seller.repository.network.Log;

public class AppLog {
    public static void events(String event, KeyValueEntity... keyValues) {
        JsonObject json = new JsonObject();
        json.addProperty("name", event);

        JsonObject data = new JsonObject();

        for (KeyValueEntity keyValue : keyValues) {
            if (keyValue.getValue() instanceof String) {
                data.addProperty(keyValue.getKey(), (String) keyValue.getValue());
            } else if (keyValue.getValue() instanceof Number) {
                data.addProperty(keyValue.getKey(), (Number) keyValue.getValue());
            } else if (keyValue.getValue() instanceof Boolean) {
                data.addProperty(keyValue.getKey(), (Boolean) keyValue.getValue());
            } else if (keyValue.getValue() instanceof Character) {
                data.addProperty(keyValue.getKey(), (Character) keyValue.getValue());
            }
        }

        if (AppUser.has()) {
            data.addProperty("userId", AppUser.get().getId());
            data.addProperty("email", AppUser.get().getEmail());
        }

        data.addProperty("platform", "ANDROID");
        json.add("data", data);

        Repository.exec(Repository.network(Log.class).events(Repository.header(json)));
    }
}
